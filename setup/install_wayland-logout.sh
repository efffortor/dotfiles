#!/bin/sh

git clone https://github.com/soreau/wayland-logout.git
cd wayland-logout || exit

echo "Build wayland-logout"
meson build --prefix /usr

echo "Install wayland-logout"
if command -v doas >/dev/null
then
  doas -- ninja -C build install
else
  sudo ninja -C build install
fi

cd ..
