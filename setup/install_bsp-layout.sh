#!/bin/sh

git clone https://github.com/phenax/bsp-layout.git
cd bsp-layout || exit

if command -v doas >/dev/null
then
  doas -- make PREFIX=/usr install
else
  sudo make PREFIX=/usr install
fi

cd ..
