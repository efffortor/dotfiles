#!/bin/sh

curl -fLo greenclip https://github.com/erebe/greenclip/releases/download/3.4/greenclip
chmod 755 greenclip
if command -v doas >/dev/null
then
  doas -- mv -fv greenclip /usr/bin/greenclip
else
  sudo mv -fv greenclip /usr/bin/greenclip
fi
