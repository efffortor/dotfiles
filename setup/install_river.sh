#!/bin/sh

# Need wlroots-devel to build

git clone https://github.com/ifreund/river.git
cd river || exit

echo "Initialize submodules"
git submodule update --init

echo "Install river"
if command -v doas >/dev/null
then
  doas -- zig build -Drelease-safe -Dxwayland --prefix /usr install
else
  sudo zig build -Drelease-safe -Dxwayland --prefix /usr install
fi

cd ..
