#!/bin/sh

echo "Shell (bash/zsh/fish): "
read -r shell
if [ -z "${shell}" ]
then exit
fi

echo "Display server (wayland/x11): "
read -r server
if [ -z "${server}" ]
then exit
fi

echo "
~~~ Installing shell config ~~~
"
case ${shell} in
  bash)
    cp -rfv ./home/.bashrc-bloated ~/.bashrc
    cp -rfv ./home/.bash_profile ~/.bash_profile
    ;;
  zsh)
    cp -rfv ./home/.bashrc ~/.bashrc
    cp -rfv ./home/.bash_profile ~/.bash_profile
    cp -rfv ./home/.config/zsh ~/.config/
    cp -rfv ./home/.local/share/zsh ~/.local/share/
    ;;
  fish)
    cp -rfv ./home/.bashrc ~/.bashrc
    cp -rfv ./home/.bash_profile ~/.bash_profile
    cp -rfv ./home/.config/fish ~/.config/
    ;;
  *)
    exit ;;
esac

echo "
~~~ Installing ${server} specified config ~~~
"
case ${server} in
  wayland)
    cp -rfv ./home/.config/foot ~/.config/
    cp -rfv ./home/.config/hikari ~/.config/
    cp -rfv ./home/.config/mako ~/.config/
    cp -rfv ./home/.config/nwg-launchers ~/.config/
    cp -rfv ./home/.config/river ~/.config/
    cp -rfv ./home/.config/sway ~/.config/
    cp -rfv ./home/.config/swaylock ~/.config/
    cp -rfv ./home/.config/waybar ~/.config/
    cp -rfv ./home/.config/wofi ~/.config/
    cp -rfv ./home/.config/wayfire.ini ~/.config/
    ;;
  x11)
    cp -rfv ./home/.config/bsp-layout ~/.config/
    cp -rfv ./home/.config/bspwm ~/.config/
    cp -rfv ./home/.config/dunst ~/.config/
    cp -rfv ./home/.config/kitty ~/.config/
    cp -rfv ./home/.config/polybar ~/.config/
    cp -rfv ./home/.config/rofi ~/.config/
    cp -rfv ./home/.config/spectrwm ~/.config/
    cp -rfv ./home/.config/sxhkd ~/.config/
    cp -rfv ./home/.config/X11 ~/.config/
    cp -rfv ./home/.config/greenclip.cfg ~/.config/
    cp -rfv ./home/.config/picom.conf ~/.config/
    cp -rfv ./home/.config/doom ~/.config/
    if [ ! -d ~/.config/emacs ]
    then
      echo "
~~~ Cloning Doom Emacs ~~~
"
      git clone https://github.com/hlissner/doom-emacs ~/.config/emacs
    fi
    ;;
  *)
    exit ;;
esac

echo "
~~~ Installing standard config ~~~
"
# Assests
mkdir -pv ~/Pictures
mkdir -pv ~/.local/share/fonts
cp -rfv ./home/Pictures/Animated ~/Pictures/
cp -rfv ./home/Pictures/Wallpapers ~/Pictures/
cp -rfv ./home/.local/share/fonts/* ~/.local/share/fonts/
# Scripts
cp -rfv ./home/.local/bin ~/.local/
# The bloated dotfiles itself
cp -rfv ./home/.config/alacritty ~/.config/
cp -rfv ./home/.config/bottom ~/.config/
cp -rfv ./home/.config/cava ~/.config/
cp -rfv ./home/.config/cointop ~/.config/
cp -rfv ./home/.config/GIMP ~/.config/
cp -rfv ./home/.config/git ~/.config/
cp -rfv ./home/.config/gtk-2.0 ~/.config/
cp -rfv ./home/.config/gtk-3.0 ~/.config/
cp -rfv ./home/.config/mpd ~/.config/
cp -rfv ./home/.config/mpDris2 ~/.config/
cp -rfv ./home/.config/ncmpcpp ~/.config/
cp -rfv ./home/.config/neofetch ~/.config/
cp -rfv ./home/.config/newsboat ~/.config/
cp -rfv ./home/.config/npm ~/.config/
cp -rfv ./home/.config/nvim ~/.config/
cp -rfv ./home/.config/qutebrowser ~/.config/
cp -rfv ./home/.config/ranger ~/.config/
cp -rfv ./home/.config/tmux ~/.config/
cp -rfv ./home/.config/tridactyl ~/.config/
cp -rfv ./home/.config/vifm ~/.config/
cp -rfv ./home/.config/youtube-dl ~/.config/
cp -rfv ./home/.config/zathura ~/.config/
cp -rfv ./home/.config/mimeapps.list ~/.config/
cp -rfv ./home/.config/pulsemixer.cfg ~/.config/
cp -rfv ./home/.config/starship.toml ~/.config/

echo "
~~~ Cloning submodules ~~~
"
[ -d ~/.config/tmux/plugins/tpm ] || git clone https://github.com/tmux-plugins/tpm ~/.config/tmux/plugins/tpm
[ -d ~/.config/ranger/plugins/ranger_devicons ] || git clone https://github.com/FollieHiyuki/ranger_devicons ~/.config/ranger/plugins/ranger_devicons

# Post deployment
mkdir -pv ~/.local/share/mpd/playlists
mkdir -pv ~/.local/share/gnupg
chmod -v 700 ~/.local/share/gnupg
chmod -v 600 ~/.local/share/gnupg/*

chsh -s /usr/bin/${shell} ${USER}

echo "
~~~ Finished ~~~"
