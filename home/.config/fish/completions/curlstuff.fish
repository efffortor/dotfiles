# curlstuff script
# Get stuff using curl

complete -x -c curlstuff -a "--weather --moon --qr --cheat --crypto --url --news --dict"
complete -x -c curlstuff -l weather -d "Weather"
complete -f -c curlstuff -l moon -d "Moon circle"
complete -x -c curlstuff -l qr -d "Get qr code"
complete -x -c curlstuff -l cheat -d "Cheatsheet"
complete -x -c curlstuff -l crypto -d "Cryptocurrency rate"
complete -x -c curlstuff -l url -d "Shorten url"
complete -x -c curlstuff -l news -d "Tech news"
complete -x -c curlstuff -l dict -d "Simple dictionary"
