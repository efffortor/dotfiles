# animatedwall script for X11
# Set live wallpapers for screens

complete -x -c animatedwall -a "--full --part --random"
complete -F -c animatedwall -l full -d "A live wallpaper for all screens"
complete -F -c animatedwall -l part -d "Set live wallpaper for each screen"
complete -f -c animatedwall -l random -d "Set random wallpapers for all screens"
