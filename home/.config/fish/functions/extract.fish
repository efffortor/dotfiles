function extract -d "Extract archives"
    set -l remove_archive
    set -l success
    set -l extract_dir

    if test -z "$argv[1]"
        echo -e "\e[1;34mUsage:\e[0m extract [-option] [file...]
\e[1;33mOptions:\e[0m
    -r, --remove    Remove archive after unpacking."
    end

    set remove_archive 1
    if [ "$argv[1]" = "-r" ] || [ "$argv[1]" = "--remove" ]
        set remove_archive 0
        set --erase argv[1]
    end

    while test -n "$argv[1]"
        if not test -f "$argv[1]"
            echo "extract: '$argv[1]' is not a valid file" >&2
            set --erase argv[1]
            continue
        end

        set success 0
        set extract_dir (echo "$argv[1]" | cut -d'.' -f1)
        switch "$argv[1]"
            case "*.tar.gz" or "*.tgz"
                tar zxvf "$argv[1]"
            case "*.tar.bz2" or "*.tbz" or "*.tbz2"
                tar xvjf "$argv[1]"
            case "*.tar.xz" or "*.txz"
                tar --xz -xvf "$argv[1]"; or xzcat "$argv[1]" | tar xvf -
            case "*.tar.zma" or "*.tlz"
                tar --lzma -xvf "$argv[1]"; or lzcat "$argv[1]" | tar xvf -
            case "*.tar.zst" or "*.tzst"
                tar --zstd -xvf "$argv[1]"; or zstdcat "$argv[1]" | tar xvf -
            case "*.tar" or "*.tar.lz"
                tar xvf "$argv[1]"
            case "*.tar.lz4"
                lz4 -c -d "$argv[1]" | tar xvf -
            case "*.tar.lrz"
                lrzuntar "$argv[1]"
            case "*.gz"
                gunzip -d "$argv[1]"
            case "*.bz2"
                bunzip2 "$argv[1]"
            case "*.xz"
                unxz "$argv[1]"
            case "*.lrz"
                lrunzip "$argv[1]"
            case "*.lz4"
                lz4 -d "$argv[1]"
            case "*.lzma"
                unlzma "$argv[1]"
            case "*.z"
                uncompress "$argv[1]"
            case "*.zip" or "*.war" or "*.jar" or "*.sublime-package" or "*.ipa" or "*.ipsw" or "*.xpi" or "*.apk" or "*.aar" or "*.whl"
                unzip "$argv[1]" -d "$extract_dir"
            case "*.rar"
                unrar x -ad "$argv[1]"
            case "*.rpm"
                mkdir "$extract_dir"; and cd "$extract_dir"; and rpm2cpio "../$argv[1]" | cpio --quiet -id; and cd ..
            case "*.7z"
                7za x "$argv[1]"
            case "*.deb"
                mkdir -p "$extract_dir/control"
                mkdir -p "$extract_dir/data"
                cd "$extract_dir"; ar vx "../$argv[1]" > /dev/null
                cd control; tar xzvf ../control.tar.gz
                cd ../data; extract ../data.tar.*
                cd ..; rm *.tar.* debian-binary
                cd ..
            case "*.zst"
                unzstd "$argv[1]"
            case "*"
                echo "extract: '$argv[1]' cannot be extracted" >&2
                set success 1
        end

        if [ "$success" = 0 ] && [ "$remove_archive" = 0 ]
            rm "$argv[1]"
        end
        set --erase argv[1]
    end
end
