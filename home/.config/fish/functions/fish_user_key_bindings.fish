bind -M insert \cu backward-kill-line
bind -M insert \ck kill-line
bind -M insert \cw backward-kill-word
bind -M insert \cb backward-word
bind -M insert \cf forward-word
bind -M insert \cs complete-and-search
bind -M insert \ca beginning-of-line
bind -M insert \ce end-of-line
bind -M insert \co accept-autosuggestion
bind -M insert \cp history-search-backward
bind -M insert \cn history-search-forward

bind -M default \cu backward-kill-line
bind -M default \ck kill-whole-line
bind -M default k history-search-backward
bind -M default j history-search-forward

# Previous command ('!!')
function bind_bang
    switch (commandline -t)
    case "!"
        commandline -t $history[1]; commandline -f repaint
    case "*"
        commandline -i '!'
    end
end
bind -M insert '!' bind_bang

# Token from previous command ('!$')
function bind_dollar
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end
bind -M insert '$' bind_dollar
