function n2 -d "Start nnn in dual pane (tmux)"
    tmux new-session -d -s nnn -n nnn "nnn -Hc"
    tmux split-window -h "nnn -Hc"
    TERM=screen-256color command tmux attach -t nnn:nnn
end
