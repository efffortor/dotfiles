# _________________               
# ___    |__  /__(_)_____ ________
# __  /| |_  /__  /_  __ `/_  ___/
# _  ___ |  / _  / / /_/ /_(__  ) 
# /_/  |_/_/  /_/  \__,_/ /____/  
#
# remap
alias doas='doas --'
abbr p pulsemixer
alias startx='startx $HOME/.config/X11/xinitrc'
alias fehwpp="feh --no-fehbg --bg-fill --randomize ~/Pictures/Wallpapers/*"
alias ls='exa -lF --icons --sort=type'
alias la='exa -laF --icons --sort=type'
alias lt='exa --tree'
alias cp='cp -vir'
alias mv='mv -vir'
alias rm='rm -vr'
alias mkdir='mkdir -pv'
abbr no 'grep -viP'
abbr latest_pkg "expac --timefmt='%Y-%m-%d %T' '%l\t%n' | sort | tail -n 30"
alias yarn='yarn --use-yarnrc $HOME/.config/yarn/config'
abbr tmux 'TERM=screen-256color command tmux'
alias cat='bat --style plain --color=always'
alias myip='curl ipinfo.io/geo'
# colorizing
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias ncdu='ncdu --color=dark'
alias ip='ip -color=always'
# vim
abbr v nvim
abbr vi 'nvim -o (fzf)'
# emacs
alias doom='$HOME/.config/emacs/bin/doom'
abbr e 'emacs -nw'
alias emacs="emacsclient -c -a 'emacs'"
# rsync
abbr rsyncdir "rsync -uavP --delete-after"
abbr rsyncfile "rsync -avP"
# youtube-dl
abbr yoump3 'youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail'
abbr youflac 'youtube-dl --extract-audio --audio-format flac'
abbr youbest 'youtube-dl -f bestvideo+bestaudio'
abbr youlist 'youtube-dl -f bestvideo+bestaudio --yes-playlist'
# git
abbr gbr 'git checkout (git branch | fzf | tr -d "*")'
# flatpak
abbr codi 'flatpak run --command=sh com.vscodium.codium'
# nnn
abbr nnn 'nnn -Hc'
alias ncp="cat $XDG_CONFIG_HOME/nnn/.selection | tr '\0' '\n'"
# zlua
alias zc='z -c'      # restrict matches to subdirs of $PWD
alias zz='z -i'      # cd with interactive selection
alias zf='z -I'      # use fzf to select in multiple matches
alias zb='z -b'      # quickly cd to the parent directory
alias zbi='z -b -i'  # interactive jump backward
alias zbf='z -b -I'  # interactive jump backward with fzf

# Lazy cd-ing
function ..    ; cd .. ; end
function ...   ; cd ../.. ; end
function ....  ; cd ../../.. ; end

# Bad hands
abbr gti git
abbr ragner ranger
abbr claer clear
abbr sduo sudo
abbr duso sudo
abbr daso doas
abbr daos doas
