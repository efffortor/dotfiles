if test -z "$XDG_RUNTIME_DIR"
  set -gx SSH_ENV $HOME/.ssh/environment
else
  set -gx SSH_ENV $XDG_RUNTIME_DIR/ssh-agent.env
end

if not pgrep -u "$USER" ssh-agent >/dev/null
  ssh-agent -c -t 1h > "$SSH_ENV"
  chmod 600 $SSH_ENV
end

if test -z "$SSH_AUTH_SOCK"
  source "$SSH_ENV" >/dev/null
end
