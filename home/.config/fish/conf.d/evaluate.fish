# Github cli completion
if command -v gh > /dev/null; and not test -f $HOME/.config/fish/completions/gh.fish
    gh completion -s fish > $HOME/.config/fish/completions/gh.fish
end

# kitten from kitty
if command -v kitty > /dev/null
  kitty + complete setup fish | source
end

# Starship prompt
if command -v starship > /dev/null
  starship init fish --print-full-init | source
end

# direnv
if command -v direnv > /dev/null
  eval (direnv hook fish)
end

# poetry
if not test -d "$POETRY_HOME"
  curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
end
if not test -f "$HOME/.config/fish/completions/poetry.fish"
  poetry completions fish > $HOME/.config/fish/completions/poetry.fish
end

# pyenv
if not test -d "$PYENV_ROOT"
  git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT
  git clone https://github.com/pyenv/pyenv-update.git $PYENV_ROOT/plugins/pyenv-update
  git clone https://github.com/pyenv/pyenv-virtualenv.git $PYENV_ROOT/plugins/pyenv-virtualenv
end
pyenv init - | source
pyenv virtualenv-init - | source

# nodenv
if not test -d "$NODENV_ROOT"
  git clone https://github.com/nodenv/nodenv.git $NODENV_ROOT
  mkdir -p $NODENV_ROOT/plugins
  git clone https://github.com/nodenv/node-build.git $NODENV_ROOT/plugins/node-build
  git clone https://github.com/nodenv/nodenv-env.git $NODENV_ROOT/plugins/nodenv-env
  git clone https://github.com/nodenv/nodenv-man.git $NODENV_ROOT/plugins/nodenv-man
  git clone https://github.com/nodenv/nodenv-aliases.git $NODENV_ROOT/plugins/nodenv-aliases
  git clone https://github.com/nodenv/nodenv-each.git $NODENV_ROOT/plugins/nodenv-each
  git clone https://github.com/nodenv/nodenv-update.git $NODENV_ROOT/plugins/nodenv-update
  git clone https://github.com/nodenv/nodenv-vars.git $NODENV_ROOT/plugins/nodenv-vars
end
nodenv init - | source

# vi key bindings
fish_vi_key_bindings

# fzf
if not test -f "$HOME/.config/fish/functions/fzf_key_bindings.fish"
  curl -sfLo $HOME/.config/fish/functions/fzf_key_bindings.fish https://raw.githubusercontent.com/junegunn/fzf/master/shell/key-bindings.fish
end
fzf_key_bindings

# z.lua
if not test -f "$HOME/.config/fish/z.lua"
  curl -sfLo $HOME/.config/fish/z.lua https://raw.githubusercontent.com/skywind3000/z.lua/master/z.lua
  mkdir -p $HOME/.local/share/zlua
end
lua $HOME/.config/fish/z.lua --init fish | source
