###############################################################
# Default flags
# You can set up flags you want broot to start with by
# default, for example `default_flags="ihp"` if you usually want
# to see hidden and gitignored files and the permissions (then
# if you don't want the hidden files you can launch `br -H`)
# A popular flag is the `g` one which displays git related info.
#
default_flags = "ghis"

###############################################################
# Special paths
# If some paths must be handled specially, uncomment (and change
# this section as per the examples
#
# [special-paths]
# "/media/slow-backup-disk" = "no-enter"
# "/home/dys/useless" = "hide"
# "/home/dys/my-link-I-want-to-explore" = "enter"

###############################################################
# Date/Time format
# If you want to change the format for date/time, uncomment the
# following line and change it according to
# https://docs.rs/chrono/0.4.11/chrono/format/strftime/index.html
#
# date_time_format = "%Y/%m/%d %R"

###############################################################
# Whether to mark the selected line with a triangle
#
show_selection_mark = true

###############################################################
# Column order
# cols_order, if specified, must be a permutation of the following
# array. You should keep the name at the end as it has a variable
# length.
#
# cols_order = [
# 	"mark",
# 	"git",
# 	"branch",
# 	"permission",
# 	"date",
# 	"size",
# 	"count",
# 	"name",
# ]

###############################################################
# True Colors
# If this parameter isn't set, broot tries to automatically
# determine whether true colors (24 bits) are available.
# As this process is unreliable, you may uncomment this setting
# and set it to false or true if you notice the colors in
# previewed images are too off.
# true_colors = false

###############################################################
# Icons
# If you want to display icons in broot, uncomment this line
# (see https://dystroy.org/broot/icons for installation and
# troubleshooting)
icon_theme = "vscode"

###############################################################
# Verbs and shortcuts
# You can define your own commands which would be applied to
# the selection.

[[verbs]]
name = "touch"
invocation = "touch {new_file}"
execution = "touch {directory}/{new_file}"
leave_broot = false

[[verbs]]
invocation = "edit"
shortcut = "e"
execution = "$EDITOR {file}"
leave_broot = false

[[verbs]]
invocation = "create {subpath}"
execution = "$EDITOR {directory}/{subpath}"
leave_broot = false

# If $PAGER isn't set on your computer, you should either set it
#  or just replace it with your viewer of choice in the 'execution'
#  pattern.
# Example:
#  execution = "less {file}"
[[verbs]]
name = "view"
invocation = "view"
execution = "$PAGER {file}"
leave_broot = false

# uncomment if you want to launch a terminal on ctrl-T
# (on exit you'll be back in broot)
[[verbs]]
invocation = "terminal"
key = "ctrl-t"
execution = "$SHELL"
set_working_dir = true
leave_broot = false

# A popular set of shorctuts for going up and down:
#
[[verbs]]
key = "ctrl-j"
execution = ":line_down"

[[verbs]]
key = "ctrl-k"
execution = ":line_up"

[[verbs]]
key = "ctrl-h"
execution = ":parent"

[[verbs]]
key = "ctrl-l"
execution = ":focus"

[[verbs]]
key = "ctrl-d"
execution = ":page_down"

[[verbs]]
key = "ctrl-u"
execution = ":page_up"

[[verbs]]
key = "ctrl-c"
execution = ":quit"

# Bookmarks for better navigation
[[verbs]]
shortcut = "fh"
execution = ":focus ~"

[[verbs]]
shortcut = "fC"
execution = ":focus ~/Code"

[[verbs]]
shortcut = "fD"
execution = ":focus ~/Downloads"

[[verbs]]
shortcut = "fP"
execution = ":focus ~/Pictures"

[[verbs]]
shortcut = "fV"
execution = ":focus ~/Videos"

[[verbs]]
shortcut = "fA"
execution = ":focus ~/Media"

[[verbs]]
shortcut = "fU"
execution = ":focus ~/Music"

[[verbs]]
shortcut = "ff"
execution = ":focus ~/.config"

[[verbs]]
shortcut = "fl"
execution = ":focus ~/.local/share"

[[verbs]]
shortcut = "fe"
execution = ":focus /etc"

[[verbs]]
shortcut = "fu"
execution = ":focus /usr/share"

[[verbs]]
shortcut = "fo"
execution = ":focus /opt"

[[verbs]]
shortcut = "fb"
execution = ":focus /boot"

[[verbs]]
shortcut = "fm"
execution = ":focus /media"

[[verbs]]
shortcut = "fM"
execution = ":focus /mnt"

[[verbs]]
shortcut = "fi"
execution = ":focus /run/media"

[[verbs]]
shortcut = "fv"
execution = ":focus /var"

[[verbs]]
shortcut = "ft"
execution = ":focus /tmp"

[[verbs]]
shortcut = "fd"
execution = ":focus /dev"

[[verbs]]
shortcut = "fS"
execution = ":focus /srv"

[[verbs]]
shortcut = "fr"
execution = ":focus /"

###############################################################
# Skin
# If you want to change the colors of broot,
# uncomment the following bloc and start messing
# with the various values.
#
[skin]
default = "None None"
tree = "ansi(94) None None"
file = "gray(20) None / gray(15) None"
directory = "None Bold None bold"
exe = "Cyan None"
link = "Magenta None"
pruning = "gray(12) None Italic"
perm__ = "gray(5) None"
perm_r = "ansi(94) None"
perm_w = "ansi(132) None"
perm_x = "ansi(65) None"
owner = "ansi(138) None"
group = "ansi(131) None"
count = "ansi(136) gray(3)"
dates = "ansi(66) None"
sparse = "ansi(214) None"
content_extract = "ansi(29) None"
content_match = "ansi(34) None"
git_branch = "ansi(229) None"
git_insertions = "ansi(28) None"
git_deletions = "ansi(160) None"
git_status_current = "gray(5) None"
git_status_modified = "ansi(28) None"
git_status_new = "ansi(94) None Bold"
git_status_ignored = "gray(17) None"
git_status_conflicted = "ansi(88) None"
git_status_other = "ansi(88) None"
selected_line = "None gray(6)"
char_match = "Yellow None"
file_error = "Red None"
flag_label = "gray(15) None"
flag_value = "ansi(208) None Bold"
input = "White None"
status_error = "gray(22) ansi(124)"
status_job = "ansi(220) gray(5)"
status_normal = "gray(20) gray(3) / gray(2) gray(2)"
status_italic = "ansi(208) gray(3) / gray(2) gray(2)"
status_bold = "ansi(208) gray(3) Bold / gray(2) gray(2)"
status_code = "ansi(229) gray(3) / gray(2) gray(2)"
status_ellipsis = "gray(19) gray(1) / gray(2) gray(2)"
purpose_normal = "gray(20) gray(2)"
purpose_italic = "ansi(178) gray(2)"
purpose_bold = "ansi(178) gray(2) Bold"
purpose_ellipsis = "gray(20) gray(2)"
scrollbar_track = "gray(7) None / gray(4) None"
scrollbar_thumb = "gray(22) None / gray(14) None"
help_paragraph = "gray(20) None"
help_bold = "ansi(208) None Bold"
help_italic = "ansi(166) None"
help_code = "gray(21) gray(3)"
help_headers = "ansi(208) None"
help_table_border = "ansi(239) None"
preview = "gray(20) gray(1) / gray(18) gray(2)"
preview_line_number = "gray(12) gray(3)"
preview_match = "None ansi(29)"
hex_null = "gray(11) None"
hex_ascii_graphic = "gray(18) None"
hex_ascii_whitespace = "ansi(143) None"
hex_ascii_other = "ansi(215) None"
hex_non_ascii = "ansi(167) None"

###############################################################
# File Extension Colors
#
# uncomment and modify the next section if you want to color
# file name depending on their extension
#
# [ext-colors]
# png = "rgb(255, 128, 75)"
# rs = "yellow"

