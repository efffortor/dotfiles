# Uncomment this to still load settings configured via autoconfig.yml
config.load_autoconfig()
config.source('base16-nord.py')
# config.source('base16-onedark.py')

# ________    _______________
# __  ___/______  /__  /___(_)_____________ ________
# _____ \_  _ \  __/  __/_  /__  __ \_  __ `/_  ___/
# ____/ //  __/ /_ / /_ _  / _  / / /  /_/ /_(__  )
# /____/ \___/\__/ \__/ /_/  /_/ /_/_\__, / /____/
#                                   /____/
# Darkmode
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.prefers_color_scheme_dark = True

# Confirm on quit
c.confirm_quit = ['downloads', 'multiple-tabs']

# Block canvas reading
c.content.canvas_reading = False

# Cookies
c.content.cookies.accept = 'no-3rdparty'
# c.content.cookies.store = True

# Screen sharing
c.content.desktop_capture = False

# Caching dns
c.content.dns_prefetch = False

# Geo location requests
c.content.geolocation = False

# Headers
c.content.headers.accept_language = 'en-US,en;q=0.5'
c.content.headers.custom = {"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"}
# c.content.headers.do_not_track = True
# c.content.headers.referer = 'same-domain'

# From current TorBrowser
c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0'

# Additional blocked hosts
# c.content.host_blocking.enabled = True
c.content.host_blocking.lists.append( str(config.configdir) + "/blockedHosts")

# Hyperlink auditing
# c.content.hyperlink_auditing = False

# JavaScript.
# c.content.javascript.alert = True
# c.content.javascript.can_access_clipboard = False
# c.content.javascript.can_close_tabs = False
# c.content.javascript.can_open_tabs_automatically = False
c.content.javascript.enabled = False

# No recording
c.content.media.audio_capture = False
c.content.media.audio_video_capture = False
c.content.media.video_capture = False

# Mouse lock
c.content.mouse_lock = False

# No notification
c.content.notifications = False

# Incognito
c.content.private_browsing = True

# SSL
c.content.ssl_strict = 'ask'

# Disable WebGL
c.content.webgl = False

# WebRTC
c.content.webrtc_ip_handling_policy = 'disable-non-proxied-udp'

# XSS auditing
# c.content.xss_auditing = False

# Download location
c.downloads.location.directory = '$HOME/Downloads'
c.downloads.location.remember = False

# Fonts
c.fonts.completion.category = '13pt Sarasa Fixed J'
c.fonts.completion.entry = '13pt Sarasa Fixed J'
c.fonts.contextmenu = '13pt Sarasa Fixed J'
c.fonts.default_family = ['Sarasa Fixed J', 'Iosevka Nerd Font', 'Noto Color Emoji']
c.fonts.default_size = '11pt'
# c.fonts.prompts = 'default_size sans-serif'
c.fonts.statusbar = '11pt Sarasa Fixed J'
c.fonts.tabs.selected = 'bold 11pt Sarasa Fixed J'
c.fonts.tabs.unselected = 'italic 11pt Sarasa Fixed J'

# Hints
c.hints.uppercase = True

# Scoll bar
# c.scrolling.bar = 'overlay'

# Tabs
c.tabs.last_close = 'default-page'
# c.tabs.new_position.related = 'next'
# c.tabs.new_position.stacking = True
c.tabs.new_position.unrelated = 'prev'
c.tabs.show = 'multiple'
c.tabs.title.alignment = 'center'
# c.tabs.title.format = '{audio}{index}: {current_title} [{scroll_pos}]'

# urls
c.url.start_pages = 'file:///home/follie/Code/Startpage/index.html'
c.url.default_page = 'file:///home/follie/Code/Startpage/index.html'
c.url.open_base_url = True
c.url.searchengines = {'DEFAULT': 'https://search.disroot.org/?category_general=on&q={}',
                       '!ddg': 'https://www.duckduckgo.com/?q={}',
                       '!searx0': 'https://search.disroot.org/?category_general=on&q={}',
                       '!searx1': 'https://searx.info/?category_general=on&q={}',
                       '!searx2': 'https://searx.fmac.xyz/?category_general=on&q={}',
                       '!searx3': 'https://searx.be/?category_general=on&q={}',
                       '!searx4': 'https://searx.monicz.pl/?category_general=on&q={}',
                       '!sp': 'https://startpage.com/do/search?query={}',
                       '!qwant': 'https://www.qwant.com/?q={}',
                       '!so': 'http://stackoverflow.com/search?q={}',
                       '!github': 'https://github.com/search?utf8=\u2713&q={}',
                       '!gitlab': 'https://gitlab.com/explore?utf8=\u2713&sort=latest_activity_desc&name={}',
                       '!mdn': 'https://developer.mozilla.org/en-US/search?q={}',
                       '!devhints': 'https://devhints.io/{}',
                       '!gentoo': 'https://wiki.gentoo.org/index.php?title=Special%3ASearch&profile=default&fulltext=Search&search={}',
                       '!arch': 'https://wiki.archlinux.org/index.php?search={}',
                       '!wiki': 'https://en.wikipedia.org/wiki/{}',
                       '!osm': 'https://www.openstreetmap.org/search?query={}',
                       '!gi': 'https://www.google.com/search?tbm=isch&q={}',
                       '!urban': 'https://www.urbandictionary.com/define.php?term={}',
                       '!thesaurus': 'https://www.thesaurus.com/browse/{}',
                       '!vocab': 'https://www.vocabulary.com/dictionary/{}',
                       '!twitter': 'https://twitter.com/search?q={}',
                       '!yt': 'https://www.youtube.com/results?search_query={}',
                       '!odysee': 'https://odysee.com/$/search?q={}',
                       '!archive': 'https://archive.org/search.php?query={}',
                       '!alto': 'https://alternativeto.net/browse/search?q={}'
}

# _____________       _____________
# ___  __ )__(_)____________  /__(_)_____________ ________
# __  __  |_  /__  __ \  __  /__  /__  __ \_  __ `/_  ___/
# _  /_/ /_  / _  / / / /_/ / _  / _  / / /  /_/ /_(__  )
# /_____/ /_/  /_/ /_/\__,_/  /_/  /_/ /_/_\__, / /____/
#                                         /____/
#
config.bind(',m', 'hint links spawn mpv {hint-url}')
config.bind(',y', 'hint links spawn alacritty -e youtube-dl {hint-url}')
config.bind(',t', 'config-cycle tabs.show multiple never')
config.bind(',T', 'set-cmd-text -s :open -t')
