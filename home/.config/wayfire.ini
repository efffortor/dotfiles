# Input configuration ──────────────────────────────────────────────────────────

[input]
xkb_layout = us,jp
xkb_options = grp:alt_shift_toggle
kb_numlock_default_state = true
cursor_theme = Breeze_Snow
cursor_size = 24
disable_touchpad_while_mouse = true
disable_touchpad_while_typing = true
natural_scroll = true

# Output configuration ─────────────────────────────────────────────────────────

[DP-2]
mode = 1920x1080@60.000000
layout = -1920,0
transform = normal
scale = 1.000000

[eDP-1]
mode = 1920x1080@60.000000
layout = 0,0
transform = normal
scale = 1.000000

[HDMI-A-1]
mode = 1920x1080@60.000000
layout = 1920,0
transform = normal
scale = 1.000000

# Core options ─────────────────────────────────────────────────────────────────

[core]

# List of plugins to be enabled.
plugins = \
  alpha \
  animate \
  autostart \
  command \
  cube \
  decoration \
  expo \
  fast-switcher \
  grid \
  idle \
  move \
  oswitch \
  place \
  resize \
  switcher \
  vswitch \
  wobbly \
  zoom \
  scale \
  window-rules \
  blur

# Close focused window.
close_top_view = <super> KEY_Q

# Workspaces arranged into a grid: 3 × 3.
vwidth = 3
vheight = 3

# Pure Wayland
# xwayland = false

# server / client decoration
preferred_decoration_mode = server

# Mouse bindings ───────────────────────────────────────────────────────────────

# Drag windows by holding down Super and left mouse button.
[move]
activate = <super> BTN_LEFT

# Resize them with right mouse button + Super.
[resize]
activate = <super> BTN_RIGHT

# Zoom in the desktop by scrolling + Super + Control.
[zoom]
modifier = <super> <ctrl>

# Change opacity by scrolling with Super.
[alpha]
modifier = <super> <alt>

# Startup commands ─────────────────────────────────────────────────────────────

[autostart]

# Automatically start wf-background, wf-panel and wf-dock
# Requires wf-shell to be installed
autostart_wf_shell = false

# Notifications
notifications = mako -c ~/.config/mako/config

# The bar
bar = waybar -c ~/.config/waybar/config-generic -s ~/.config/waybar/style-generic.css

# Wallpaper
wallpaper = wlwpp

# Screen color temperature
# Note: Requires Redshift with Wayland support.
# redshift = redshift -m wayland

# Idle configuration
idle = swayidle -w before-sleep swaylock

# XDG desktop portal
# Needed by some GTK applications
# portal = /usr/libexec/xdg-desktop-portal

pipewire = pipewire

# Music
mpd = mpd
mpDris2 = mpDris2

# Set GTK theme
gtk_theme = gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Dark'
icon_theme = gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'

[idle]
toggle = <super> KEY_Z
screensaver_timeout = 300
dpms_timeout = 600

# Applications ─────────────────────────────────────────────────────────────────

[command]

# Start a terminal
binding_terminal = <super> KEY_ENTER
command_terminal = alacritty
binding_foot = <super> <shift> KEY_ENTER
command_foot = foot

# Start your launcher
binding_launcher = <super> KEY_D
command_launcher = nwggrid -b 2e3440 -o 0.85

# Logout screen
binding_logout = <super> KEY_ESC
command_logout = nwgbar -b 2e3440 -o 0.85

# Screenshots
binding_screenshot = KEY_PRINT
command_screenshot = screenshot --region
binding_screenshot_interactive = <super> KEY_PRINT
command_screenshot_interactive = screenshot --full

# Volume controls
repeatable_binding_volume_up = KEY_VOLUMEUP
command_volume_up = volumecontrol increase && pkill -x -RTMIN+11 waybar
repeatable_binding_volume_down = KEY_VOLUMEDOWN
command_volume_down = volumecontrol decrease && pkill -x -RTMIN+11 waybar
binding_mute = KEY_MUTE
command_mute = volumecontrol toggle && pkill -x -RTMIN+11 waybar

# Media keys
binding_play = KEY_PLAYPAUSE
command_play = playerctl play-pause
binding_next = KEY_NEXTSONG
command_next = playerctl next
binding_prev = KEY_PREVIOUSSONG
command_prev = playerctl previous

# Screen brightness
repeatable_binding_light_up = KEY_BRIGHTNESSUP
command_light_up = brightness up
repeatable_binding_light_down = KEY_BRIGHTNESSDOWN
command_light_down = brightness down

# Windows ──────────────────────────────────────────────────────────────────────

# Position the windows in certain regions of the output.
[grid]
#
# ⇱ ↑ ⇲   │ 7 8 9
# ← f →   │ 4 5 6
# ⇱ ↓ ⇲ d │ 1 2 3 0
# ‾   ‾
slot_bl = <super> KEY_KP1
slot_b = <super> KEY_KP2
slot_br = <super> KEY_KP3
slot_l = <super> KEY_LEFT | <super> KEY_KP4
slot_c = <super> KEY_UP | <super> KEY_KP5
slot_r = <super> KEY_RIGHT | <super> KEY_KP6
slot_tl = <super> KEY_KP7
slot_t = <super> KEY_KP8
slot_tr = <super> KEY_KP9
# Restore default.
restore = <super> KEY_DOWN | <super> KEY_KP0

# Change active window with an animation.
[switcher]
next_view = <alt> KEY_TAB
prev_view = <alt> <shift> KEY_TAB

# Simple active window switcher.
[fast-switcher]
activate = <alt> KEY_ESC

# Animations
[animate]
close_animation = zoom
open_animation = zoom
enabled_for = (type is "toplevel" | (type is "x-or" & focusable is true))

# Blur
[blur]
method = kawase
kawase_degrade = 2
kawase_iterations = 2
kawase_offset = 5

# Scale view
[scale]
toggle = <super> KEY_P
toggle_all = <super> <shift> KEY_P
interact = false
middle_click_close = true

# Workspaces ───────────────────────────────────────────────────────────────────

# Switch to workspace.
[vswitch]
binding_left = <ctrl> <super> KEY_LEFT
binding_down = <ctrl> <super> KEY_DOWN
binding_up = <ctrl> <super> KEY_UP
binding_right = <ctrl> <super> KEY_RIGHT
# Move the focused window with the same key-bindings, but add Shift.
binding_win_left = <ctrl> <super> <shift> KEY_LEFT
binding_win_down = <ctrl> <super> <shift> KEY_DOWN
binding_win_up = <ctrl> <super> <shift> KEY_UP
binding_win_right = <ctrl> <super> <shift> KEY_RIGHT

# Show the current workspace row as a cube.
[cube]
activate = <ctrl> <alt> BTN_LEFT
background_mode = simple
# Switch to the next or previous workspace.
#rotate_left = <super> <ctrl> KEY_H
#rotate_right = <super> <ctrl> KEY_L

# Show an overview of all workspaces.
[expo]
toggle = <super>
# Select a workspace.
# Workspaces are arranged into a grid of 3 × 3.
# The numbering is left to right, line by line.
#
# ⇱ k ⇲
# h ⏎ l
# ⇱ j ⇲
# ‾   ‾
# See core.vwidth and core.vheight for configuring the grid.
select_workspace_1 = KEY_1
select_workspace_2 = KEY_2
select_workspace_3 = KEY_3
select_workspace_4 = KEY_4
select_workspace_5 = KEY_5
select_workspace_6 = KEY_6
select_workspace_7 = KEY_7
select_workspace_8 = KEY_8
select_workspace_9 = KEY_9

# Outputs ──────────────────────────────────────────────────────────────────────

# Change focused output.
[oswitch]
# Switch to the next output.
next_output = <super> KEY_O
# Same with the window.
next_output_with_win = <super> <shift> KEY_O

# Rules ────────────────────────────────────────────────────────────────────────

[window-rules]
alacritty_opacity = on created if app_id is "Alacritty" then set alpha 0.9
foot_opacity = on created if app_id is "foot" then set alpha 0.9

# You can get the properties of your applications with the following command:
# $ WAYLAND_DEBUG=1 alacritty 2>&1
