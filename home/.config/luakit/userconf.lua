-- Settings
local settings = require "settings"

settings.window.home_page = "file:///home/follie/Code/Startpage/index.html"
settings.completion.history.order = "title"
settings.webview.default_charset = "utf-8"
settings.webview.enable_caret_browsing = true
settings.webview.enable_developer_extras = true
settings.webview.user_agent = "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0"

local engines = settings.window.search_engines
engines.duckduckgo = "https://www.duckduckgo.com/?q=%s"
engines.searx0 = "https://search.disroot.org/?category_general=on&q=%s"
engines.searx1 = "https://searx.info/?category_general=on&q=%s"
engines.searx2 = "https://searx.fmac.xyz/?category_general=on&q=%s"
engines.searx3 = "https://searx.be/?category_general=on&q=%s"
engines.searx4 = "https://searx.monicz.pl/?category_general=on&q=%s"
engines.startpage = "https://startpage.com/do/search?query=%s"
engines.qwant = "https://www.qwant.com/?q=%s"
engines.stackoverflow = "http://stackoverflow.com/search?q=%s"
engines.github = "https://github.com/search?utf8=✓&q=%s"
engines.gitlab = "https://gitlab.com/explore?utf8=✓&sort=latest_activity_desc&name=%s"
engines.mdn = "https://developer.mozilla.org/en-US/search?q=%s"
engines.devhints = "https://devhints.io/%s"
engines.gentoowiki = "https://wiki.gentoo.org/index.php?title=Special%3ASearch&profile=default&fulltext=Search&search=%s"
engines.archwiki = "https://wiki.archlinux.org/index.php?search=%s"
engines.wiki = "https://en.wikipedia.org/wiki/%s"
engines.osm = "https://www.openstreetmap.org/search?query=%s"
engines.gi = "https://www.google.com/search?tbm=isch&q=%s"
engines.urban = "https://www.urbandictionary.com/define.php?term=%s"
engines.thesaurus = "https://www.thesaurus.com/browse/%s"
engines.vocab = "https://www.vocabulary.com/dictionary/%s"
engines.twitter = "https://twitter.com/search?q=%s"
engines.odysee = "https://odysee.com/$/search?q=%s"
engines.yt = "https://www.youtube.com/results?search_query=%s"
engines.archive = "https://archive.org/search.php?query=%s"
engines.alto = "https://archive.org/search.php?query=%s"
engines.default = engines.searx0

local downloads = require "downloads"
downloads.default_dir = os.getenv("HOME") .. "/Downloads"

local noscript = require "noscript"
noscript.enable_scripts = false

require_web_module("referer_control_wm")

-- Bindings
local modes = require "modes"

modes.add_binds("normal", {{
    "<control-c>",
    "Copy selected text.",
    function ()
        luakit.selection.clipboard = luakit.selection.primary
    end
}})

modes.add_binds("normal", {{
    "<control-shift-c>",
    "Stop loading the current tab.",
    function (w)
        w.view:stop()
    end
}})
