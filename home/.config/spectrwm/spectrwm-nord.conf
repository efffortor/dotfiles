# ________                  _____
# __  ___/____________________  /__________      ________ ___
# _____ \___  __ \  _ \  ___/  __/_  ___/_ | /| / /_  __ `__ \
# ____/ /__  /_/ /  __/ /__ / /_ _  /   __ |/ |/ /_  / / / / /
# /____/ _  .___/\___/\___/ \__/ /_/    ____/|__/ /_/ /_/ /_/
#        /_/
#
# General config
workspace_limit	             = 10
focus_mode		               = default
focus_close		               = previous
focus_close_wrap	           = 1
focus_default		             = last
spawn_position		           = last
workspace_clamp	             = 1
warp_focus		               = 1
warp_pointer		             = 1

# Window Decoration
border_width		             = 2
color_focus		               = rgb:81/a1/c1
color_focus_maximized	       = rgb:81/a1/c1
color_unfocus		             = rgb:23/34/40
color_unfocus_maximized	     = rgb:23/34/40
region_padding	             = 10
tile_gap		                 = 10

# Workspace names
name                         = ws[1]:
name                         = ws[2]:
name                         = ws[3]:
name                         = ws[4]:
name                         = ws[5]:
name                         = ws[6]:
name                         = ws[7]:
name                         = ws[8]:
name                         = ws[9]:
name                         = ws[10]:

# Autostart
autorun                      = ws[1]:mpDris2

# Region containment
# Distance window must be dragged/resized beyond the region edge before it is
# allowed outside the region.
boundary_width 		           = 50

# Remove window border when bar is disabled and there is only one window in workspace
disable_border		           = 0

# Bar Settings
bar_enabled		               = 1
bar_border_width	           = 0
bar_border[1]		             = rgb:81/a1/c1
bar_border_unfocus[1]	       = rgb:2e/34/40
bar_color[1]		             = rgb:2e/34/40, rgb:bf/61/6a, rgb:a3/be/8c, rgb:d0/87/70, rgb:eb/cb/8b, rgb:81/a1/c1, rgb:b4/8e/ad, rgb:88/c0/d0, rgb:d8/de/e9
bar_color_selected[1]	       = rgb:81/a1/c1
bar_font_color[1]	           = rgb:2e/34/40, rgb:bf/61/6a, rgb:eb/cb/8b, rgb:81/a1/c1, rgb:d8/de/e9
bar_font_color_selected	     = rgb:2e/34/40
bar_font		                 = Iosevka Nerd Font:style=Regular:size=13:antialias=true, Sarasa Fixed J:style=Regular:size=13:antialias=true
bar_action		               = ~/.config/spectrwm/baraction.sh
bar_action_expand	           = 1
bar_justify		               = left
bar_format                   = +|T +@bg=4;(+L)+@bg=0; +@bg=2;  +S +@bg=0; +@bg=5;  +M +A +@bg=1;+@fg=0;  %a,%d %b %Y |  %H:%M +@bg=0;
workspace_indicator	         = listcurrent,listactive,markcurrent,listurgent
bar_at_bottom		             = 0
stack_enabled		             = 1
clock_enabled		             = 1
clock_format		             = %a, %d %B %Y %H:%M
iconic_enabled	             = 1
maximize_hide_bar	           = 0
window_class_enabled	       = 0
window_instance_enabled	     = 0
window_name_enabled	         = 0
verbose_layout		           = 1
urgent_enabled		           = 0
urgent_collapse	             = 0

# Dialog box size ratio when using TRANSSZ quirk; 0.3 < dialog_ratio <= 1.0
dialog_ratio		             = 0.6

# Mod key, (Windows key is Mod4) (Apple key on OSX is Mod2)
modkey                       = Mod4

# QUIRKS
quirk[Firefox:Dialog]        = FLOAT
quirk[Element]               = WS[7]
quirk[TelegramDesktop]       = WS[7]
quirk[Zathura]               = WS[4]
quirk[mpv]                   = WS[6]
quirk[krita]                 = FLOAT
quirk[Inkscape]              = FLOAT
quirk[Gimp-2.10]             = FLOAT
quirk[Gimp]                  = FLOAT
quirk[Thunderbird]           = WS[3]
quirk[Emacs:emacs]           = IGNORESPAWNWS
quirk[Komikku]               = FLOAT

# Clear all default key bindings
keyboard_mapping             = /dev/null
# Key bindings
bind[bar_toggle]             = MOD+b
bind[bar_toggle_ws]          = MOD+Shift+b
bind[button2]                = MOD+v
bind[cycle_layout]           = MOD+space
bind[flip_layout]	           = MOD+Shift+backslash
bind[float_toggle]	         = MOD+f
bind[focus_main]	           = MOD+m
bind[focus_next]	           = MOD+j
bind[focus_prev]	           = MOD+k
bind[focus_urgent]	         = MOD+u
bind[height_grow]	           = MOD+Shift+equal
bind[height_shrink]	         = MOD+Shift+minus
bind[iconify]		             = MOD+w
bind[master_add]	           = MOD+Shift+comma
bind[master_del]	           = MOD+Shift+period
bind[master_grow]	           = MOD+l
bind[master_shrink]	         = MOD+h
bind[maximize_toggle]	       = MOD+e
bind[move_down]		           = MOD+Shift+bracketright
bind[move_left]		           = MOD+bracketleft
bind[move_right]	           = MOD+bracketright
bind[move_up]		             = MOD+Shift+bracketleft
bind[mvrg_1]		             = MOD+Shift+KP_End
bind[mvrg_2]		             = MOD+Shift+KP_Down
bind[mvrg_3]		             = MOD+Shift+KP_Next
bind[mvrg_4]		             = MOD+Shift+KP_Left
bind[mvrg_5]		             = MOD+Shift+KP_Begin
bind[mvrg_6]		             = MOD+Shift+KP_Right
bind[mvrg_7]		             = MOD+Shift+KP_Home
bind[mvrg_8]		             = MOD+Shift+KP_Up
bind[mvrg_9]		             = MOD+Shift+KP_Prior
bind[mvws_1]		             = MOD+Shift+1
bind[mvws_2]		             = MOD+Shift+2
bind[mvws_3]		             = MOD+Shift+3
bind[mvws_4]		             = MOD+Shift+4
bind[mvws_5]		             = MOD+Shift+5
bind[mvws_6]		             = MOD+Shift+6
bind[mvws_7]		             = MOD+Shift+7
bind[mvws_8]		             = MOD+Shift+8
bind[mvws_9]		             = MOD+Shift+9
bind[mvws_10]		             = MOD+Shift+0
bind[quit]		               = MOD+MOD1+q
bind[raise_toggle]	         = MOD+Control+r
bind[restart]		             = MOD+Shift+r
bind[rg_1]		               = MOD+KP_End
bind[rg_2]		               = MOD+KP_Down
bind[rg_3]		               = MOD+KP_Next
bind[rg_4]		               = MOD+KP_Left
bind[rg_5]		               = MOD+KP_Begin
bind[rg_6]		               = MOD+KP_Right
bind[rg_7]		               = MOD+KP_Home
bind[rg_8]		               = MOD+KP_Up
bind[rg_9]		               = MOD+KP_Prior
bind[rg_next]		             = MOD+period
bind[rg_prev]		             = MOD+comma
bind[search_win]	           = MOD+Shift+f
bind[search_workspace]	     = MOD+slash
bind[stack_dec]		           = MOD+Control+period
bind[stack_inc]		           = MOD+Control+comma
bind[stack_reset]	           = MOD+Control+space
bind[swap_next]		           = MOD+Shift+j
bind[swap_prev]		           = MOD+Shift+k
bind[uniconify]		           = MOD+Shift+w
bind[version]		             = MOD+Shift+v
bind[width_grow]	           = MOD+equal
bind[width_shrink]	         = MOD+minus
bind[wind_del]		           = MOD+q
bind[wind_kill]		           = MOD+Shift+q
bind[ws_1]		               = MOD+1
bind[ws_2]		               = MOD+2
bind[ws_3]		               = MOD+3
bind[ws_4]		               = MOD+4
bind[ws_5]		               = MOD+5
bind[ws_6]		               = MOD+6
bind[ws_7]		               = MOD+7
bind[ws_8]		               = MOD+8
bind[ws_9]		               = MOD+9
bind[ws_10]		               = MOD+0
bind[ws_next]		             = MOD+Right
bind[ws_next_all]	           = MOD+Up
bind[ws_next_move]	         = MOD+Shift+Up
bind[ws_prev]		             = MOD+Left
bind[ws_prev_all]	           = MOD+Down
bind[ws_prev_move]	         = MOD+Shift+Down
bind[ws_prior]		           = MOD+a

# Validated default programs:
program[term]                = alacritty
bind[term]                   = MOD+Return

program[dlauncher]		       = launchermenu --dmenu
bind[dlauncher]              = MOD+d

program[dmenu]               = dmenu_run
bind[dmenu]                  = MOD+Control+d

program[winswitch]           = dwinswitch
bind[winswitch]              = MOD+Control+w

program[greenclip]           = clipboard --dmenu
bind[greenclip]              = MOD+MOD1+d

program[clipdel]             = clipboard --clear
bind[clipdel]                = MOD+MOD1+r

program[buku-dmenu]          = bukumenu --dmenu
bind[buku-dmenu]             = MOD+Control+s

program[dwebsearch]          = searchmenu --dmenu
bind[dwebsearch]             = MOD+s

program[touchpad_toggle]     = touchpad_toggle
bind[touchpad_toggle]        = XF86TouchpadToggle

program[lightup]             = brightness up
bind[lightup]                = XF86MonBrightnessUp

program[lightdown]           = brightness down
bind[lightdown]              = XF86MonBrightnessDown

program[play]                = playerctl play-pause
bind[play]                   = XF86AudioPlay

program[next]                = playerctl next
bind[next]                   = XF86AudioNext

program[prev]                = playerctl previous
bind[prev]                   = XF86AudioPrev

program[audioup]             = volumecontrol increase
bind[audioup]                = XF86AudioRaiseVolume

program[audiodown]           = volumecontrol decrease
bind[audiodown]              = XF86AudioLowerVolume

program[audiomute]           = volumecontrol toggle
bind[audiomute]              = XF86AudioMute

program[scrot_all]	         = screenshot --full
bind[scrot_all]              = MOD+Print

program[scrot]	             = screenshot --region
bind[scrot]                  = Print

program[emacs]               = emacsclient -c -a 'emacs'
bind[emacs]                  = MOD+MOD1+e
