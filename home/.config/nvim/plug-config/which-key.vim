autocmd! User vim-which-key call which_key#register('<Space>', "g:which_key_map")
" Map leader to which_key
nnoremap <silent> <leader> :silent <c-u> :silent WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual '<Space>'<CR>

" Create map to add keys to
let g:which_key_map =  {}
" No floating
let g:which_key_use_floating_win = 0
" Define a separator
let g:which_key_sep = '→'

set timeoutlen=300

" Highlighting
highlight default link WhichKey          Operator
highlight default link WhichKeySeperator DiffAdded
highlight default link WhichKeyGroup     Identifier
highlight default link WhichKeyDesc      Function

" Hide statusline
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 noshowmode ruler

" 1-level entries
let g:which_key_map['.'] = [':e $MYVIMRC'                                   , 'open init']
let g:which_key_map['v'] = ['<C-W>v'                                        , 'split right']
let g:which_key_map['='] = ['<C-W>='                                        , 'balance windows']
let g:which_key_map['h'] = ['<C-W>s'                                        , 'split below']
let g:which_key_map[';'] = [':Commands'                                     , 'commands']
let g:which_key_map['e'] = [':Files'                                        , 'search files']
let g:which_key_map['n'] = [':CocCommand explorer'                          , 'coc-explorer']
" let g:which_key_map['n'] = [':NERDTreeToggle'                               , 'nerdtree']
let g:which_key_map['u'] = [':UndotreeToggle'                               , 'undotree']
let g:which_key_map['?'] = 'coc search'
let g:which_key_map['d'] = 'doge'
let g:which_key_map['T'] = 'tableize range'

" File managers
let g:which_key_map.r = {
      \ 'name' : '+FileManagers' ,
      \ 'e' : [':Explore'                        , 'netrw'],
      \ 'l' : [':FloatermNew lf'                 , 'lf'],
      \ 'n' : [':NnnPicker'                      , 'nnn'],
      \ 'r' : [':RnvimrToggle'                   , 'ranger'],
      \ 'v' : [':Vifm'                           , 'vifm'],
      \ }

" Actions
let g:which_key_map.a = {
      \ 'name' : '+Actions' ,
      \ 'a' : 'align text',
      \ 'b' : [':Bracey'                                      , 'live server'],
      \ 'B' : [':BraceyStop'                                  , 'live server stop'],
      \ 'c' : [':ColorizerToggle'                             , 'colorizer toggle'],
      \ 'd' : [':RainbowDelim'                                , 'rainbow csv on'],
      \ 'D' : [':NoRainbowDelim'                              , 'rainbow csv off'],
      \ 'e' : [':ALEDisableBuffer'                            , 'disable linting'],
      \ 'E' : [':ALEEnableBuffer'                             , 'enable linting'],
      \ 'g' : [':Goyo'                                        , 'goyo toggle'],
      \ 'i' : [':IndentLinesToggle'                           , 'indent toggle'],
      \ 'l' : [':RainbowAlign'                                , 'align csv columns'],
      \ 'L' : [':RainbowShrink'                               , 'shrink csv columns'],
      \ 'm' : [':MarkdownPreview'                             , 'markdown preview'],
      \ 'M' : [':MarkdownPreviewStop'                         , 'markdown preview stop'],
      \ 'p' : [':DelimitMateSwitch'                           , 'auto-pairs toggle'],
      \ 'r' : [':RainbowToggle'                               , 'rainbow brackets toggle'],
      \ 's' : [':CocCommand cSpell.toggleEnableSpellChecker'  , 'spellchecker toggle'],
      \ 'v' : [':Vista!!'                                     , 'vista toggle'],
      \ 'V' : [':Vista finder coc'                            , 'vista coc tag'],
      \ 'w' : [':ToggleWhitespace'                            , 'whitespace toggle'],
      \ 'W' : [':StripWhitespace'                             , 'strip whitespace'],
      \ }

" Buffer/Tab commands
let g:which_key_map.b = {
      \ 'name' : '+Buffer/Tab' ,
      \ '/' : ['Buffers'                                  , 'fzf buffer'],
      \ 'e' : ['enew'                                     , 'new empty buffer'],
      \ 'E' : ['tabnew'                                   , 'new tab'],
      \ 'd' : ['bd'                                       , 'delete buffer'],
      \ 'D' : ['tabclose'                                 , 'tab close'],
      \ 'f' : ['bfirst'                                   , 'first buffer'],
      \ 'F' : ['tabfirst'                                 , 'first tab'],
      \ 'l' : ['blast'                                    , 'last buffer'],
      \ 'L' : ['tablast'                                  , 'last tab'],
      \ 'n' : ['bnext'                                    , 'next buffer'],
      \ 'N' : ['tabnext'                                  , 'next tab'],
      \ 'p' : ['bprevious'                                , 'previous buffer'],
      \ 'P' : ['tabprevious'                              , 'previous tab'],
      \ 'v' : [':Bdelete menu'                            , 'delete buffers (verbose)'],
      \ }

" fzf
let g:which_key_map.f = {
      \ 'name' : '+Fzf' ,
      \ '/' : [':History/'              , 'history'],
      \ ';' : [':Commands'              , 'commands'],
      \ 'a' : [':Ag'                    , 'text Ag'],
      \ 'b' : [':BLines'                , 'current buffer'],
      \ 'B' : [':Buffers'               , 'open buffers'],
      \ 'c' : [':Commits'               , 'commits'],
      \ 'C' : [':BCommits'              , 'buffer commits'],
      \ 'f' : [':Files!'                , 'files'],
      \ 'g' : [':GFiles'                , 'git files'],
      \ 'G' : [':GFiles?'               , 'modified git files'],
      \ 'h' : [':History'               , 'file history'],
      \ 'H' : [':History:'              , 'command history'],
      \ 'l' : [':Lines'                 , 'lines'] ,
      \ 'm' : [':Marks'                 , 'marks'] ,
      \ 'M' : [':Maps'                  , 'normal maps'] ,
      \ 'p' : [':Helptags'              , 'help tags'] ,
      \ 'P' : [':Tags'                  , 'project tags'],
      \ 's' : [':CocList snippets'      , 'snippets'],
      \ 'S' : [':Colors'                , 'color schemes'],
      \ 't' : [':Rg'                    , 'text Rg'],
      \ 'T' : [':BTags'                 , 'buffer tags'],
      \ 'w' : [':Windows'               , 'search windows'],
      \ 'y' : [':Filetypes'             , 'file types'],
      \ }

" Sessions (Startify/Dashboard)
let g:which_key_map.s = {
      \ 'name' : '+Session' ,
      \ 'c' : [':SClose'          , 'close session'],
      \ 'd' : [':SDelete'         , 'delete session'],
      \ 'l' : [':SLoad'           , 'load session'],
      \ 's' : [':SSave'           , 'save session'],
      \ }
" let g:which_key_map.s = {
"      \ 'name' : '+Session' ,
"      \ 'l' : [':SessionLoad'           , 'load session'],
"      \ 's' : [':SessionSave'           , 'save session'],
"      \ }

" Git commands
let g:which_key_map.g = {
      \ 'name' : '+Git' ,
      \ 'a' : [':Git add .'                        , 'add all'],
      \ 'A' : [':Git add %'                        , 'add current'],
      \ 'b' : [':Git blame'                        , 'blame'],
      \ 'B' : [':GBrowse'                          , 'browse'],
      \ 'c' : [':Git commit'                       , 'commit'],
      \ 'd' : [':Git diff'                         , 'diff'],
      \ 'D' : [':Gdiffsplit'                       , 'diff split'],
      \ 'f' : ['<Plug>(GitGutterFold)'             , 'fold'],
      \ 'g' : [':GGrep'                            , 'git grep'],
      \ 'h' : ['<Plug>(GitGutterPreviewHunk)'      , 'preview hunk'],
      \ 'H' : ['<Plug>(GitGutterStageHunk)'        , 'stage hunk'],
      \ 'j' : ['<Plug>(GitGutterNextHunk)'         , 'next hunk'],
      \ 'k' : ['<Plug>(GitGutterPrevHunk)'         , 'prev hunk'],
      \ 'l' : [':Git log'                          , 'log'],
      \ 'L' : [':GitGutterLineHighlightsToggle'    , 'lines toggle'],
      \ 'm' : ['<Plug>(git-messenger)'             , 'git messages'],
      \ 'p' : [':Git push'                         , 'push'],
      \ 'P' : [':Git pull'                         , 'pull'],
      \ 'r' : [':GRemove'                          , 'remove'],
      \ 's' : [':Gstatus'                          , 'status'],
      \ 't' : [':GitGutterSignsToggle'             , 'signs toggle'],
      \ 'T' : [':GitGutterLineNrHighlightsToggle'  , 'linenr toggle'],
      \ 'u' : ['<Plug>(GitGutterUndoHunk)'         , 'undo hunk'],
      \ 'v' : [':GV'                               , 'view commits'],
      \ 'V' : [':GV!'                              , 'view buffer commits'],
      \ }

" Coc commands
let g:which_key_map.l = {
      \ 'name' : '+CoC' ,
      \ '.' : [':CocConfig'                          , 'config'],
      \ ';' : ['<Plug>(coc-refactor)'                , 'refactor'],
      \ 'a' : 'code action',
      \ 'b' : [':CocNext'                            , 'next action'],
      \ 'B' : [':CocPrev'                            , 'prev action'],
      \ 'c' : [':CocList commands'                   , 'commands'],
      \ 'd' : ['<Plug>(coc-definition)'              , 'definition'],
      \ 'D' : ['<Plug>(coc-declaration)'             , 'declaration'],
      \ 'e' : [':CocList extensions'                 , 'extensions'],
      \ 'f' : ['<Plug>(coc-format-selected)'         , 'format selected'],
      \ 'F' : ['<Plug>(coc-format)'                  , 'format'],
      \ 'h' : ['<Plug>(coc-float-hide)'              , 'hide'],
      \ 'i' : ['<Plug>(coc-implementation)'          , 'implementation'],
      \ 'I' : [':CocList diagnostics'                , 'diagnostics'],
      \ 'j' : ['<Plug>(coc-float-jump)'              , 'float jump'],
      \ 'l' : ['<Plug>(coc-codelens-action)'         , 'code lens'],
      \ 'n' : ['<Plug>(coc-diagnostic-next)'         , 'next diagnostic'],
      \ 'N' : ['<Plug>(coc-diagnostic-next-error)'   , 'next error'],
      \ 'o' : ['<Plug>(coc-openlink)'                , 'open link'],
      \ 'O' : [':CocList outline'                    , 'outline'],
      \ 'p' : ['<Plug>(coc-diagnostic-prev)'         , 'prev diagnostic'],
      \ 'P' : ['<Plug>(coc-diagnostic-prev-error)'   , 'prev error'],
      \ 'q' : ['<Plug>(coc-fix-current)'             , 'quickfix'],
      \ 'r' : ['<Plug>(coc-rename)'                  , 'rename'],
      \ 'R' : ['<Plug>(coc-references)'              , 'references'],
      \ 's' : [':CocList -I symbols'                 , 'references'],
      \ 'S' : [':CocList snippets'                   , 'snippets'],
      \ 't' : ['<Plug>(coc-type-definition)'         , 'type definition'],
      \ 'u' : [':CocListResume'                      , 'resume list'],
      \ 'U' : [':CocUpdate'                          , 'update CoC'],
      \ 'z' : [':CocDisable'                         , 'disable CoC'],
      \ 'Z' : [':CocEnable'                          , 'enable CoC'],
      \ }

" Floaterm
let g:which_key_map.m = {
      \ 'name' : '+Terminal' ,
      \ ';' : [':FloatermNew --wintype=normal --height=10'      , 'terminal'],
      \ 'g' : [':FloatermNew lazygit'                           , 'git'],
      \ 'd' : [':FloatermNew lazydocker'                        , 'docker'],
      \ 'n' : [':FloatermNew node'                              , 'node'],
      \ 'p' : [':FloatermNew python'                            , 'python'],
      \ 't' : [':FloatermToggle'                                , 'toggle'],
      \ 's' : [':FloatermNew ncdu --color=dark'                 , 'ncdu'],
      \ 'c' : [':FloatermNew calcurse'                          , 'calcurse'],
      \ }

" Vim wiki
let g:which_key_map.w = {
      \ 'name' : '+Wiki' ,
      \ 'i' : 'diary index',
      \ 's' : 'UI select',
      \ 't' : 'tab index',
      \ 'w' : 'index',
      \ }

" Figlet/Toilet
let g:which_key_map.i = {
    \ 'name' : '+Figlet' ,
    \ 's'    : [':.!figlet -f standard'            , 'standard'],
    \ 'l'    : [':.!figlet -f slant'               , 'slant'],
    \ 'b'    : [':.!figlet -f banner'              , 'banner'],
    \ 'e'    : [':.!figlet -f lean'                , 'lean'],
    \ 'p'    : [':.!figlet -f speed'               , 'speed'],
    \ 'r'    : [':.!figlet -f roman'               , 'roman'],
    \ 'd'    : [':.!figlet -f doh'                 , 'doh'],
    \ 'w'    : [':.!figlet -f starwars'            , 'starwars'],
    \ 'm'    : [':.!figlet -f morse'               , 'morse'],
    \ 'x'    : [':.!toilet -f term -F border'      , 'border'],
    \ }

" Table mode
let g:which_key_map.t = {
      \ 'name' : '+TableMode' ,
      \ 'm' : 'toggle table mode',
      \ 'r' : 'realign',
      \ 't' : 'tableize',
      \ 'd' : 'delete row',
      \ 'c' : 'delete column',
      \ 'i' : 'insert column',
      \ 'k' : 'insert column before cursor',
      \ 'f' : 'add formula',
      \ 'e' : 'evaluate formula line',
      \ }

" cheat.sh
let g:which_key_map.K = {
      \ 'name' : '+Cheatsheet',
      \ }
