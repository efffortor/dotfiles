let g:airline#extensions#ale#enabled = 0
let g:ale_disable_lsp = 1
let g:ale_warn_about_trailing_blank_lines = 1
let g:ale_warn_about_trailing_whitespace = 1
let g:ale_virtualtext_cursor = 0
let g:ale_sign_highlight_linenrs = 0
let g:ale_sign_error = ' '
let g:ale_sign_warning = ' '
let g:ale_sign_info = ''
let g:ale_sign_style_error = '>>'
let g:ale_sign_style_warning = '⚠ '
let g:ale_sign_column_always = 0
let g:ale_sign_priority = 10
let g:ale_lint_on_enter = 1
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
" let g:ale_linters_explicit = 1
let g:ale_linters = {
\   'python': ['pyright', 'bandit', 'isort', 'yapf', 'autopep8', 'black'],
\}
let g:ale_fix_on_save = 0
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\}
" let g:ale_echo_msg_error_str = 'E'
" let g:ale_echo_msg_warning_str = 'W'
" let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

" Keybindings
nmap <silent> [g <Plug>(ale_previous_wrap)
nmap <silent> ]g <Plug>(ale_next_wrap)
