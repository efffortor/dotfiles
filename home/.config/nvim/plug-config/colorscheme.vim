" Cleaner OneDark
let g:onedark_hide_endofbuffer = 1
let g:onedark_terminal_italics = 1
" Nord
let g:nord_cursor_line_number_background = 0
let g:nord_uniform_diff_background = 1
let g:nord_italic = 1
let g:nord_italic_comments = 1
let g:nord_bold = 1
let g:nord_bold_vertical_split_line = 0
let g:nord_underline = 1

colorscheme nord
