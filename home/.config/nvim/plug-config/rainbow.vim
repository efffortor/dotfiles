" Rainbow parentheses
let g:rainbow_active = 0
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}'], ['<', '>']]
" let g:rainbow_conf = {'guis': ['bold']}

" Rainbow csv
let g:disable_rainbow_key_mappings = 1
