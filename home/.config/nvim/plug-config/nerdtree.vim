" Don't open new buffer on Nerdtree buffer
autocmd BufEnter * if bufname('#') =~# "^NERD_tree_" && winnr('$') > 1 | b# | endif
" Auto close when only Nerdtree exists
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Startscreen
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter *
"   \  if argc() != 0 || exists("s:std_in")
"   \|   NERDTree
"   \| endif
"   \| wincmd p

" Nerdtree
let g:NERDTreeGitStatusUseNerdFonts = 1
let g:NERDTreeGitStatusConcealBrackets = 0 " Do not enable this when using vim_devicons
let g:NERDTreeGitStatusShowClean = 0
let g:NERDTreeGitStatusUntrackedFilesMode = 'normal'
let g:NERDTreeGitStatusShowIgnored = 1
let g:NERDTreeGitStatusWithFlags = 1
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }
let NERDTreeShowHidden=1
let g:NERDTreeHijackNetrw = 0
let g:NERDTreeDirArrowExpandable = '' " ''
let g:NERDTreeDirArrowCollapsible = '' " ''
let g:plug_window = 'noautocmd vertical topleft new' " Avoid nerdtree crash when using vim-plug
" Nerdtree devicons
let g:WebDevIconsDisableDefaultFolderSymbolColorFromNERDTreeDir = 1
let g:WebDevIconsDisableDefaultFileSymbolColorFromNERDTreeFile = 1
let g:NERDTreeLimitedSyntax = 1
let g:NERDTreeHighlightCursorline = 0
let g:NERDTreeHighlightFolders = 1
let g:NERDTreeHighlightFoldersFullName = 1
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1
