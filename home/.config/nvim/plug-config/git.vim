" Git-messenger
let g:git_messenger_no_default_mappings=1
" Gitgutter
let g:gitgutter_enabled = 1
let g:gitgutter_map_keys = 0
let g:gitgutter_highlight_linenrs = 1
let g:gitgutter_highlight_lines = 0
let g:gitgutter_signs = 0
let g:gitgutter_sign_priority = 10
let g:gitgutter_sign_allow_clobber = 0

set foldtext=gitgutter#fold#is_changed()

" OneDark/Nord
if g:colors_name == "onedark"
  highlight GitGutterAddLineNr ctermfg=2 guifg=#98c379
  highlight GitGutterChangeLineNr ctermfg=3 guifg=#e5c07b
  highlight GitGutterDeleteLineNr ctermfg=1 guifg=#e06c75
  highlight GitGutterChangeDeleteLineNr ctermfg=5 guifg=#c678dd
elseif g:colors_name == "nord"
  highlight GitGutterAddLineNr ctermfg=2 guifg=#a3be8c
  highlight GitGutterChangeLineNr ctermfg=3 guifg=#ebcb8b
  highlight GitGutterDeleteLineNr ctermfg=1 guifg=#bf616a
  highlight GitGutterChangeDeleteLineNr ctermfg=5 guifg=#b48ead
endif
