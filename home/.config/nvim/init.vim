" Basic settings
source $HOME/.config/nvim/general/basic-settings.vim

" polyglot_disabled needs to be set before polyglot being loaded (-_-)
source $HOME/.config/nvim/plug-config/polyglot.vim

" Now load plugins
source $HOME/.config/nvim/vim-plug/vim-plug.vim

" Load vim plugins' settings
source $HOME/.config/nvim/plug-config/moving.vim " Quickscope colors need to be loaded before setting colorscheme
source $HOME/.config/nvim/plug-config/colorscheme.vim " Load theme first, then everything else
" source $HOME/.config/nvim/plug-config/airline.vim
source $HOME/.config/nvim/plug-config/ale.vim
source $HOME/.config/nvim/plug-config/cheat_sh.vim
source $HOME/.config/nvim/plug-config/choosewin.vim
source $HOME/.config/nvim/plug-config/closetag.vim
source $HOME/.config/nvim/plug-config/coc.vim
source $HOME/.config/nvim/plug-config/dashboard.vim
source $HOME/.config/nvim/plug-config/delimitMate.vim
source $HOME/.config/nvim/plug-config/doc.vim
source $HOME/.config/nvim/plug-config/doge.vim
source $HOME/.config/nvim/plug-config/easy-align.vim
source $HOME/.config/nvim/plug-config/emmet.vim
source $HOME/.config/nvim/plug-config/far.vim
source $HOME/.config/nvim/plug-config/floaterm.vim
source $HOME/.config/nvim/plug-config/fzf.vim
source $HOME/.config/nvim/plug-config/git.vim
source $HOME/.config/nvim/plug-config/goyo.vim
source $HOME/.config/nvim/plug-config/illuminate.vim
source $HOME/.config/nvim/plug-config/indentline.vim
" source $HOME/.config/nvim/plug-config/nerdtree.vim
" source $HOME/.config/nvim/plug-config/nnn.vim
source $HOME/.config/nvim/plug-config/rainbow.vim
source $HOME/.config/nvim/plug-config/rnvimr.vim
source $HOME/.config/nvim/plug-config/rooter.vim
source $HOME/.config/nvim/plug-config/spaceline.vim
" source $HOME/.config/nvim/plug-config/startify.vim
source $HOME/.config/nvim/plug-config/tablemode.vim
source $HOME/.config/nvim/plug-config/undo.vim
source $HOME/.config/nvim/plug-config/vim-buffet.vim
source $HOME/.config/nvim/plug-config/vimwiki.vim
source $HOME/.config/nvim/plug-config/vista.vim
source $HOME/.config/nvim/plug-config/which-key.vim
source $HOME/.config/nvim/plug-config/whitespace.vim

" Load lua plugins' settings
lua require('_colorizer')
