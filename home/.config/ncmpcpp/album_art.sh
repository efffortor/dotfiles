#!/bin/bash

MUSIC_DIR=$HOME/Music/
COVER=/tmp/cover.jpg

{
	file="$(mpc --format %file% current -p 6600)"
	album_dir="${file%/*}"
	[[ -z "$album_dir" ]] && exit 1
	album_dir="$MUSIC_DIR/$album_dir"
	covers="$(find "$album_dir" -type d -exec find {} -maxdepth 1 -type f -iregex ".*/.*\(Album\|album\|Cover\|cover\|Folder\|folder\|Artwork\|artwork\|Front\|front\).*[.]\(jpe?g\|png\|gif\|bmp\)" \; )"
	src="$(echo -n "$covers" 2>/dev/null | head -n1)"
	rm -f "$COVER" 

	# For Notifications
	if [[ -n "$src" ]] ; then
		# Resize the image's width to 64px
		convert "$src" -resize 64x "$COVER"
		if [[ -f "$COVER" ]] ; then
			notify-send -u low -i ${COVER} " Now Playing" "$(mpc --format '%title% \n%artist% - %album%' current)"
		fi
	else
		notify-send -u low -i $HOME/.config/dunst/headphones.png " Now Playing" "$(mpc --format '%title% \n%artist% - %album%' current)"
	fi
} &
