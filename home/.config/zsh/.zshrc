# Install Zinit if missing
[ -d $HOME/.local/share/zsh/zinit/bin ] || git clone https://github.com/zdharma/zinit.git $HOME/.local/share/zsh/zinit/bin
# Load zinit
source $HOME/.local/share/zsh/zinit/bin/zinit.zsh

# Load ls_colors
autoload -U colors && colors

# disable in favor of zinit built-in method to add completions
[ -d $HOME/.local/share/zsh/completions ] || mkdir -p $HOME/.local/share/zsh/completions
# fpath+=$HOME/.local/share/zsh/completions

# Initialize completion
autoload -Uz compinit
zmodload zsh/complist
compinit -d $HOME/.local/share/zsh/zcompdump-$ZSH_VERSION

# Fun zsh stuff
autoload -U zmv zcalc zargs zed

# github-cli completion
if command -v gh > /dev/null && [ ! -f $HOME/.local/share/zsh/completions/_gh ]; then
  gh completion -s zsh > $HOME/.local/share/zsh/completions/_gh
fi
zinit ice blockf as"completion"
zinit snippet $HOME/.local/share/zsh/completions/_gh

# kitten from kitty
if command -v kitty > /dev/null; then
  kitty + complete setup zsh | source /dev/stdin
fi

# starship prompt
if command -v starship > /dev/null; then
  eval "$(starship init zsh --print-full-init)"
fi

# direnv
if command -v direnv > /dev/null; then
  eval "$(direnv hook zsh)"
fi

# poetry
if [ ! -d $POETRY_HOME ]; then
  curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
fi
if [ ! -f $HOME/.local/share/zsh/completions/_poetry ]; then
  poetry completions zsh > $HOME/.local/share/zsh/completions/_poetry
fi
zinit ice blockf as"completion"
zinit snippet $HOME/.local/share/zsh/completions/_poetry

# pyenv
if [ ! -d $PYENV_ROOT ]; then
   git clone https://github.com/pyenv/pyenv.git $PYENV_ROOT
   git clone https://github.com/pyenv/pyenv-virtualenv.git $PYENV_ROOT/plugins/pyenv-virtualenv
   git clone https://github.com/pyenv/pyenv-update.git $PYENV_ROOT/plugins/pyenv-update
fi
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# nodenv
if [ ! -d $NODENV_ROOT ]; then
  git clone https://github.com/nodenv/nodenv.git $NODENV_ROOT
  mkdir -p $NODENV_ROOT/plugins
  git clone https://github.com/nodenv/node-build.git $NODENV_ROOT/plugins/node-build
  git clone https://github.com/nodenv/nodenv-env.git $NODENV_ROOT/plugins/nodenv-env
  git clone https://github.com/nodenv/nodenv-man.git $NODENV_ROOT/plugins/nodenv-man
  git clone https://github.com/nodenv/nodenv-aliases.git $NODENV_ROOT/plugins/nodenv-aliases
  git clone https://github.com/nodenv/nodenv-each.git $NODENV_ROOT/plugins/nodenv-each
  git clone https://github.com/nodenv/nodenv-update.git $NODENV_ROOT/plugins/nodenv-update
  git clone https://github.com/nodenv/nodenv-vars.git $NODENV_ROOT/plugins/nodenv-vars
fi
eval "$(nodenv init -)"

# autostart ssh-agent
if [[ -z "$XDG_RUNTIME_DIR" ]]; then
  export SSH_ENV=$HOME/.ssh/environment
else
  export SSH_ENV="$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
  ssh-agent -t 1h > "$SSH_ENV"
  chmod 600 $SSH_ENV
fi
if [[ -z "$SSH_AUTH_SOCK" ]]; then
  source "$SSH_ENV" >/dev/null
fi

# Additional configurations (shamelessly taken from https://github.com/hlissner/dotfiles)
source $ZDOTDIR/config.zsh
source $ZDOTDIR/completion.zsh
source $ZDOTDIR/keybinds.zsh
source $ZDOTDIR/aliases.zsh

# ______________              _____
# ___  __ \__  /___  ________ ___(_)______________
# __  /_/ /_  /_  / / /_  __ `/_  /__  __ \_  ___/
# _  ____/_  / / /_/ /_  /_/ /_  / _  / / /(__  )
# /_/     /_/  \__,_/ _\__, / /_/  /_/ /_//____/
#                     /____/
# Some OMZ stuff
zinit wait lucid for \
    OMZ::lib/clipboard.zsh \
    OMZ::plugins/extract
zinit ice blockf as"completion" for \
    OMZ::plugins/docker/_docker \
    OMZ::plugins/rust/_rust \
    OMZ::plugins/fd/_fd

# fzf key bindings and zsh completions for some commands
zinit snippet https://github.com/junegunn/fzf/blob/master/shell/key-bindings.zsh
zinit snippet https://github.com/junegunn/fzf/blob/master/shell/completion.zsh
export FZF_COMPLETION_OPTS='-x'

# https://cht.sh (cheatsheet)
zinit ice mv":cht.sh -> cht.sh" atclone"chmod +x cht.sh" as"program"
zinit snippet https://cht.sh/:cht.sh
# And its completion
zinit ice mv":zsh -> _cht" as"completion"
zinit snippet https://cheat.sh/:zsh

# Better way to pass colors than asci codes (need to be loaded before other plugins)
zinit light zpm-zsh/colors

# fzf as tab-completion (need to be loaded before zsh-autosuggestions and fast-syntax-highlighting)
zinit light Aloxaf/fzf-tab

# Fish-like history suggestion (loaded on demanded for faster startup)
zinit ice wait lucid atload'_zsh_autosuggest_start'
zinit light zsh-users/zsh-autosuggestions

# More completions
zinit wait lucid blockf light-mode for \
  zsh-users/zsh-completions \
  lukechilds/zsh-better-npm-completion
# zinit light Aloxaf/gencomp

# Syntax
zinit ice wait lucid atinit"ZINIT[COMPINIT_OPTS]=-C; zicompinit; zicdreplay"
zinit light zdharma/fast-syntax-highlighting

# Fish-like history search
zinit light zsh-users/zsh-history-substring-search

# Autopair plugin from the doomer
zinit light hlissner/zsh-autopair

# Git thingy
zinit light wfxr/forgit
# zinit light kazhala/dotbare
zinit light unixorn/git-extra-commands

# Abbreviations
zinit light olets/zsh-abbr
# zinit light djui/alias-tips

# cd around faster
zinit light skywind3000/z.lua

# Plugins' configurations
source $ZDOTDIR/plugins.zsh

# _________                __________
# __  ____/__________________  /___(_)_____________ _
# _  / __ __  ___/  _ \  _ \  __/_  /__  __ \_  __ `/
# / /_/ / _  /   /  __/  __/ /_ _  / _  / / /  /_/ /
# \____/  /_/    \___/\___/\__/ /_/  /_/ /_/_\__, /
#                                           /____/
$HOME/.local/bin/bunny
