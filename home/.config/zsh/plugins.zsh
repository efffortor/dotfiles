# Use fd instead of find for fzf_completion
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}

# Auto run chosen command from history search
fzf-history-widget-accept() {
  fzf-history-widget
  zle accept-line
}
zle -N fzf-history-widget-accept
bindkey '^X^R' fzf-history-widget-accept

# zsh-history-substring-search
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey -M viins '^N' history-substring-search-down
bindkey -M viins '^P' history-substring-search-up
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

# zsh-autosuggestions
bindkey '^O' autosuggest-accept

# fzf-tab
zstyle ':fzf-tab:*' single-group ''
zstyle ':fzf-tab:*' continuous-trigger '/'
zstyle ':fzf-tab:*' print-query alt-enter
zstyle ':fzf-tab:complete:_zlua:*' query-string input
zstyle ':fzf-tab:complete:kill:argument-rest' fzf-preview 'ps --pid=$word -o cmd --no-headers -w -w'
zstyle ':fzf-tab:complete:kill:argument-rest' fzf-flags '--preview-window=down:3:wrap'
zstyle ':fzf-tab:complete:kill:*' popup-pad 0 3
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
zstyle ':fzf-tab:complete:cd:*' popup-pad 30 0
zstyle ":completion:*:git-checkout:*" sort false
# zstyle ":fzf-tab:*" fzf-flags '--color=bg+:23'
# zstyle ':fzf-tab:*' fzf-command ftb-tmux-popup

# zsh-autopairs
AUTOPAIR_PAIRS+=("<" ">")
bindkey "<" autopair-insert
# prevents breakage in isearch
bindkey -M isearch "<" self-insert
