# remap
alias doas="doas --"
alias p="pulsemixer"
alias startx="startx $HOME/.config/X11/xinitrc"
alias fehwpp="feh --no-fehbg --bg-fill --randomize ~/Pictures/Wallpapers/*"
alias ls="exa -lF --icons --sort=type"
alias la="exa -laF --icons --sort=type"
alias lt="exa --tree"
alias cp="cp -vir"
alias mv="mv -vir"
alias rm="rm -vr"
alias mkdir="mkdir -pv"
alias no="grep -viP"
alias wttr="curl wttr.in"
alias latest_pkg="expac --timefmt='%Y-%m-%d %T' '%l\t%n' | sort | tail -n 30"
alias yarn="yarn --use-yarnrc $HOME/.config/yarn/config"
alias tmux="TERM=screen-256color tmux"
alias cat="bat --style plain --color=always"
alias myip="curl ipinfo.io/geo"
# colorizing
alias grep="grep --color=auto"
alias fgrep="fgrep --color=auto"
alias egrep="egrep --color=auto"
alias diff="diff --color=auto"
alias ncdu="ncdu --color=dark"
alias ip="ip -color=always"
# vim
alias v="nvim"
alias vi='nvim -o "$(fzf)"'
# emacs
alias doom="$HOME/.config/emacs/bin/doom"
alias e="emacs -nw"
alias emacs="emacsclient -c -a 'emacs'"
# rsync
alias rsyncdir="rsync -uavP --delete-after"
alias rsyncfile="rsync -avP"
# youtube-dl
alias yoump3="youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail"
alias youflac="youtube-dl --extract-audio --audio-format flac"
alias youbest="youtube-dl -f bestvideo+bestaudio"
alias youlist="youtube-dl -f bestvideo+bestaudio --yes-playlist"
# git
alias gbr='git checkout $(git branch | fzf | tr -d "*")'
# flatpak
alias codi='flatpak run --command=sh com.vscodium.codium'
# nnn
alias nnn="nnn -Hc"
alias ncp="cat ${NNN_SEL:-${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection} | tr '\0' '\n'"

# Lazy cd-ing
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# colorize man pages
man() {
    LESS_TERMCAP_mb=$'\e[01;31m' \
    LESS_TERMCAP_md=$'\e[01;36m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man $@
}

# Search zshall for a special keyword
zman() {
    PAGER="less -g -s '+/^       "$1"'" man ${2:-zshall}
}

# nnn with cd on quit
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The default behaviour is to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, remove the "export" as in:
    #     NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    # NOTE: NNN_TMPFILE is fixed, should not be modified
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# nnn in dual pane mode (tmux)
n2() {
  tmux new-session -d -s nnn -n nnn "nnn -Hc"
  tmux split-window -h "nnn -Hc"
  TERM=screen-256color tmux attach -t nnn:nnn
}
