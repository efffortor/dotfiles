#!/bin/bash

# Terminate already running bar instances
pkill polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar
polybar --reload -c $HOME/.config/polybar/nord/config.ini mainbar &

monitor=$(xrandr --query | grep 'HDMI-2')
if [[ $monitor = *connected* ]]; then
	polybar --reload -c $HOME/.config/polybar/nord/config.ini sidebar &
fi

echo "Polybar launched..."
