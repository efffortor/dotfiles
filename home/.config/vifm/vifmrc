" ___    ______________
" __ |  / /__(_)__  __/______ ___
" __ | / /__  /__  /_ __  __ `__ \
" __ |/ / _  / _  __/ _  / / / / /
" _____/  /_/  /_/    /_/ /_/ /_/
"
" Command used to edit files in various contexts.  The default is vim.
" If you would like to use another vi clone such as Elvis or Vile
" you will need to change this setting.
set vicmd=nvim

" Preview by default
view
" set previewoptions=graphicsdelay:50000,hardgraphicsclear

" This makes vifm perform file operations on its own instead of relying on
" standard utilities like `cp`.  While using `cp` and alike is a more universal
" solution, it's also much slower when processing large amounts of files and
" doesn't support progress measuring.
set syscalls

" Position indicator on the left
set number

" Trash Directory
" The default is to move files that are deleted with dd or :d to
" the trash directory.  If you change this you will not be able to move
" files by deleting them and then using p to put the file in the new location.
" I recommend not changing this until you are familiar with vifm.
" This probably shouldn't be an option.
set trash

" This is how many directories to store in the directory history.
set history=50

" Automatically resolve symbolic links on l or Enter.
set nofollowlinks

" With this option turned on you can run partially entered commands with
" unambiguous beginning using :! (e.g. :!Te instead of :!Terminal or :!Te<tab>).
set fastrun

" Natural sort of (version) numbers within text.
set sortnumbers

" Maximum number of changes that can be undone.
set undolevels=100

" Use Vim's format of help file (has highlighting and "hyperlinks").
" If you would rather use a plain text help file set novimhelp.
set vimhelp

" If you would like to run an executable file when you
" press return on the file name set this.
set norunexec

" Selected color scheme
colorscheme nord

" Format for displaying time in file list. For example:
" TIME_STAMP_FORMAT=%m/%d-%H:%M
" See man date or man strftime for details.
set timefmt=%Y-%m-%d\ %H:%M

" Show list of matches on tab completion in command-line mode
set wildmenu

" Display completions in a form of popup with descriptions of the matches
set wildstyle=popup

" Show dotfiles
set dotfiles

" No ../
set dotdirs=treeleafsparent

" Show number of sub-directories
set dirsize=nitems

" Display suggestions in normal, visual and view modes for keys, marks and
" registers (at most 5 files).  In other view, when available.
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers

" Ignore case in search patterns unless it contains at least one uppercase letter
set ignorecase
set smartcase

" Case insensitve path completion
set caseoptions=p

" Don't highlight search results automatically
set nohlsearch

" Use increment searching (search while typing)
set incsearch

" Try to leave some space from cursor to upper/lower border in lists
set scrolloff=4

" Don't do too many requests to slow file systems
set slowfs=curlftpfs

" Miller mode
" set millerview
" set milleroptions=lsize:1,csize:3

" Set custom status line look
set statusline=" %t%[ -> %T%]%= %A %10u:%-7g %5E  %5a %20d "

" Set custom ruler line look
set rulerformat='%2l/%S%[ +%x%]'

" What should be saved automatically between vifm sessions
set vifminfo=state,dhistory,chistory,shistory,phistory,bookmarks,bmarks

" Use ripgrep instead of grep
set grepprg='rg --column --line-number --no-heading --smart-case %i %a %s'

" ------------------------------------------------------------------------------

" :com[mand][!] command_name action
" The following macros can be used in a command
" %a is replaced with the user arguments.
" %c the current file under the cursor.
" %C the current file under the cursor in the other directory.
" %f the current selected file, or files.
" %F the current selected file, or files in the other directory.
" %b same as %f %F.
" %d the current directory name.
" %D the other window directory name.
" %m run the command in a menu window

command! df df -h %m 2> /dev/null
command! diff vim -d %f %F
command! zip zip -r %a.zip %f
command! tar tar cfv %a.tar %f
command! atool atool -x %f
command! run !! ./%f
command! make !!make %a
command! mkcd :mkdir %a | cd %a
command! vgrep vim "+grep %a"
command! reload :write | restart

" fzf file
command! fzf : let $FZF_RES = term('fzf --preview "$HOME/.local/bin/preview {}" 2>&0')
            \| if $FZF_RES != ''
            \|     goto $FZF_RES
            \| endif

" fzf dir
command! fzd : let $FZF_RES = term('fd --type d --follow --hidden --exclude .git | fzf --preview "tree -La 1 -C --dirsfirst {}" 2> /dev/tty')
            \| if $FZF_RES != ''
            \|     cd $FZF_RES
            \| endif

" ------------------------------------------------------------------------------

mark h ~/
mark C ~/Code
mark D ~/Downloads
mark P ~/Pictures
mark V ~/Videos
mark A ~/Media
mark U ~/Music
mark f ~/.config
mark l ~/.local/share
mark e /etc
mark u /usr/share
mark o /opt
mark b /boot
mark m /media
mark M /mnt
mark i /run/media
mark v /var
mark t /tmp
mark d /dev
mark s /srv
mark r /
" mark k /bedrock/strata

" ------------------------------------------------------------------------------

" filextype * xdg-open

" Pdf
filextype *.pdf zathura %c %i &,
filetype *.pdf fbpdf, fimgs, fbgs

" PostScript
filextype *.ps,*.eps,*.ps.gz
        \ zathura %f,
filetype *.ps,*,eps fimgs, fbgs

" Djvu, comics, epub
filextype *.mobi,*.pdb ebook-viewer, FBReader
filextype *.djvu,*.epub,*.cbr,*.cbz,*.cbt,*.cb7
        \ zathura %f,

" Audio
filetype *.wav,*.mp3,*.flac,*.m4a,*.wma,*.ape,*.ac3,*.og[agx],*.spx,*.opus
       \ ffplay -nodisp -autoexit %c,

" Video
filextype *.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
         \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
         \*.as[fx]
        \ mpv %f &,

" Web
filextype *.html,*.htm,*.xhtml
        \ firefox %f &,
filetype *.html,*.htm,*.xhtml lynx

" Object
filetype *.o nm %f | less

" Man page
filetype *.[1-8] man ./%c

" Images
filextype *.bmp,*.jpg,*.jpeg,*.png,*.gif,*.xpm
        \ imv %f &,
        \ feh %f &
filextype *.svg
        \ inkscape %f &,

" OpenRaster
filextype *.ora
        \ krita %f &,

" Mindmap
filextype *.vym
        \ vym %f &,

" MD5
filetype *.md5
       \ md5sum -c %f %S,

" SHA1
filetype *.sha1
       \ sha1sum -c %f %S,

" SHA256
filetype *.sha256
       \ sha256sum -c %f %S,

" SHA512
filetype *.sha512
       \ sha512sum -c %f %S,

" GPG signature
filetype *.asc
       \ !!gpg --verify %c,

" Torrent
filetype *.torrent rtorrent %f &

" FuseZipMount
filetype *.zip,*.jar,*.war,*.ear,*.oxt,*.apkg
       \ {Mount with fuse-zip}
       \ FUSE_MOUNT|fuse-zip %SOURCE_FILE %DESTINATION_DIR,
       \ {View contents}
       \ zip -sf %c | less,
       \ {Extract here}
       \ tar -xf %c,

" ArchiveMount
filetype *.tar,*.tar.bz2,*.tbz2,*.tgz,*.tar.gz,*.tar.xz,*.txz
       \ {Mount with archivemount}
       \ FUSE_MOUNT|archivemount %SOURCE_FILE %DESTINATION_DIR,

" Rar2FsMount and rar archives
filetype *.rar
       \ {Mount with rar2fs}
       \ FUSE_MOUNT|rar2fs %SOURCE_FILE %DESTINATION_DIR,

" IsoMount
filetype *.iso
       \ {Mount with fuseiso}
       \ FUSE_MOUNT|fuseiso %SOURCE_FILE %DESTINATION_DIR,

" SshMount
filetype *.ssh
       \ {Mount with sshfs}
       \ FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR %FOREGROUND,

" FtpMount
filetype *.ftp
       \ {Mount with curlftpfs}
       \ FUSE_MOUNT2|curlftpfs -o ftp_port=-,,disable_eprt %PARAM %DESTINATION_DIR %FOREGROUND,

" Fuse7z and 7z archives
filetype *.7z
       \ {Mount with fuse-7z}
       \ FUSE_MOUNT|fuse-7z %SOURCE_FILE %DESTINATION_DIR,

" Office files
filextype *.odt,*.doc,*.docx,*.sxw,*.xls,*.xlsx,*.ods,*.odp,*.pptx libreoffice %f &

" TuDu files
filetype *.tudu tudu -f %c

" Qt projects
filextype *.pro qtcreator %f &

" mbox
filetype *.mbox neomutt -f

" Directories
filextype */
        \ pcmanfm %f &,
filextype [\.]*/
        \ pcmanfm %f &,

" ------------------------------------------------------------------------------

" if $TERM == "foot"
"         fileviewer <image/*>,<video/*>,<application/font*>,<application/*opentype>,<font/*>
"                 \ preview_sixel %c %pw %ph %pd 2>/dev/null
" elseif $TERM == "xterm-kitty"
"         fileviewer <image/*>,<video/*>,<application/font*>,<application/*opentype>,<font/*>
"                 \ preview_kitty %c %px %py %pw %ph %pc kitty icat --silent --clear %pd 2>/dev/null
" endif

" fileviewer <*>
"         \ preview_vifm %c:p 2>/dev/null

" ------------------------------------------------------------------------------

" Sample mappings

" Start shell in current directory
nnoremap s :shell<cr>

" Display sorting dialog
nnoremap S :sort<cr>

" Toggle visibility of preview window
nnoremap w :view<cr>
vnoremap w :view<cr>gv

" Open file in the background using its default program
nnoremap gb :file &<cr>l

" goto current directory outside vifm
nnoremap gw :cd $PWD<cr>
" go to project root
nnoremap gr :cd $PWD<cr>

" Yank current directory path into the clipboard
nnoremap yd :!echo %d | xclip %i<cr>
" Yank current file path into the clipboard
nnoremap yf :!echo %c:p | xclip %i<cr>

" Mappings for faster renaming
nnoremap I cw<c-a>
nnoremap cc cw<c-u>
nnoremap A cw

" q to quit
nnoremap q :q<cr>

" Open console in current directory
nnoremap ,t :!alacritty &<cr>

" Open editor to edit vifmrc and apply settings after returning to vifm
nnoremap ,c :write | edit $MYVIFMRC | restart<cr>

" Toggle wrap setting on ,w key
nnoremap ,w :set wrap!<cr>

" Example of standard two-panel file managers mappings
nnoremap <f3> :!less %f<cr>
nnoremap <f4> :edit<cr>
nnoremap <f5> :copy<cr>
nnoremap <f6> :move<cr>
nnoremap <f7> :mkdir<space>
nnoremap <f8> :delete<cr>

" tabs
nnoremap <C-t> :tabnew<cr>

" map for fzf commands
nnoremap <C-p> :fzf<cr>
nnoremap <C-n> :fzd<cr>

" ------------------------------------------------------------------------------

" Icons
set classify=' :dir:/, :exe:,| :fifo:, :reg:, :link:,? :?:, ::../::'
set classify+='=:sock:'
set classify+='-:char:'
set classify+='+:block:'

set classify+=' ::.Xdefaults,,.Xresources,,.bashprofile,,.bash_profile,,.bashrc,,.dmrc,,.d_store,,.fasd,,.gitconfig,,.gitignore,,.jack-settings,,.mime.types,,.nvidia-settings-rc,,.pam_environment,,.profile,,.recently-used,,.selected_editor,,.xinitpurc,,.zprofile,,.yarnc,,.snclirc,,.tmux.conf,,.urlview,,.config,,.ini,,.user-dirs.dirs,,.mimeapps.list,,.offlineimaprc,,.msmtprc,,.Xauthority,,config::'
set classify+=' ::dropbox::'
set classify+=' ::favicon.*,,README,,readme::'
set classify+=' ::.vim,,.vimrc,,.gvimrc,,.vifm::'
set classify+=' ::gruntfile.coffee,,gruntfile.js,,gruntfile.ls::'
set classify+=' ::gulpfile.coffee,,gulpfile.js,,gulpfile.ls::'
set classify+=' ::ledger::'
set classify+=' ::license,,copyright,,copying,,LICENSE,,COPYRIGHT,,COPYING::'
set classify+=' ::node_modules::'
set classify+=' ::react.jsx::'

set classify+='λ ::*.ml,,*.mli::'
set classify+=' ::*.styl::'
set classify+=' ::*.scss::'
set classify+=' ::*.py,,*.pyc,,*.pyd,,*.pyo::'
set classify+=' ::*.php::'
set classify+=' ::*.markdown,,*.md::'
set classify+=' ::*.json::'
set classify+=' ::*.js::'
set classify+=' ::*.bmp,,*.gif,,*.ico,,*.jpeg,,*.jpg,,*.png,,*.svg,,*.svgz,,*.tga,,*.tiff,,*.xmb,,*.xcf,,*.xpm,,*.xspf,,*.xwd,,*.cr2,,*.dng,,*.3fr,,*.ari,,*.arw,,*.bay,,*.crw,,*.cr3,,*.cap,,*.data,,*.dcs,,*.dcr,,*drf,,*.eip,,*.erf,,*.fff,,*.gpr,,*.iiq,,*.k25,,*.kdc,,*.mdc,,.*mef,,*.mos,,.*.mrw,,.*.obm,,*.orf,,*.pef,,*.ptx,,*.pxn,,*.r3d,,*.raf,,*.raw,,*.rwl,,*.rw2,,*.rwz,,*.sr2,,*.srf,,*.srf,,*.srw,,*.tif,,*.x3f::'
set classify+=' ::*.ejs,,*.htm,,*.html,,*.slim,,*.xml::'
set classify+=' ::*.mustasche::'
set classify+=' ::*.css,,*.less,,*.bat,,*.conf,,*.ini,,*.rc,,*.yml,,*.cfg,,*.rc::'
set classify+=' ::*.rss::'
set classify+=' ::*.coffee::'
set classify+=' ::*.twig::'
set classify+=' ::*.c++,,*.cc,,*.c,,*.cpp,,*.cxx,,*.c,,*.h::'
set classify+=' ::*.hs,,*.lhs::'
set classify+=' ::*.lua::'
set classify+=' ::*.jl::'
set classify+=' ::*.go::'
set classify+=' ::*.ts::'
set classify+=' ::*.db,,*.dump,,*.sql::'
set classify+=' ::*.sln,,*.suo::'
set classify+=' ::*.exe::'
set classify+=' ::*.diff,,*.sum,,*.md5,,*.sha512::'
set classify+=' ::*.scala::'
set classify+=' ::*.java,,*.jar::'
set classify+=' ::*.xul::'
set classify+=' ::*.clj,,*.cljc::'
set classify+=' ::*.pl,,*.pm,,*.t::'
set classify+=' ::*.cljs,,*.edn::'
set classify+=' ::*.rb::'
set classify+=' ::*.fish,,*.sh,,*.bash,,*.zsh::'
set classify+=' ::*.dart::'
set classify+=' ::*.f#,,*.fs,,*.fsi,,*.fsscript,,*.fsx::'
set classify+=' ::*.rlib,,*.rs::'
set classify+=' ::*.d::'
set classify+=' ::*.erl,,*.hrl::'
set classify+=' ::*.ai::'
set classify+=' ::*.psb,,*.psd::'
set classify+=' ::*.jsx::'
set classify+=' ::*.vim,,*.vimrc::'
set classify+=' ::*.aac,,*.anx,,*.asf,,*.au,,*.axa,,*.flac,,*.m2a,,*.m4a,,*.mid,,*.midi,,*.mp3,,*.mpc,,*.oga,,*.ogg,,*.ogx,,*.ra,,*.ram,,*.rm,,*.spx,,*.wav,,*.wma,,*.ac3::'
set classify+=' ::*.avi,,*.flv,,*.mkv,,*.mov,,*.mov,,*.mp4,,*.mpeg,,*.mpg,,*.webm::'
set classify+=' ::*.epub,,*.pdf,,*.fb2,,*.djvu::'
set classify+=' ::*.7z,,*.apk,,*.bz2,,*.cab,,*.cpio,,*.deb,,*.gem,,*.gz,,*.gzip,,*.lh,,*.lzh,,*.lzma,,*.rar,,*.rpm,,*.tar,,*.tgz,,*.xz,,*.zip,,*.zstd::'
set classify+=' ::*.cbr,,*.cbz::'
set classify+=' ::*.log::'
set classify+=' ::*.doc,,*.docx,,*.adoc,,*.odt::'
set classify+=' ::*.xls,,*.xlsx,,*.xlsmx,,*.ods::'
set classify+=' ::*.pptx,,*.ppt,,*.odp::'

" special dir icons
set classify+=' ::.git/,,*.git/,,.cfg/::/,
             \  ::.cache/::/,
             \  ::.config/::/,
             \  ::.local/::/,
             \  ::Android/::/,
             \  ::Archives/::/,
             \  ::Books/::/,
             \  ::Code/::/,
             \  ::Desktop/::/,
             \  ::Documents/::/,
             \  ::Downloads/::/,
             \  ::Mail/::/,
             \  ::Music/::/,
             \  ::Notes/::/,
             \  ::Pictures/::/,
             \  ::Projects/::/,
             \  ::Public/::/,
             \  ::Template/::/,
             \  ::Unity/::/,
             \  ::Videos/::/'
