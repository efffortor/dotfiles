;; Font settings
(setq doom-font (font-spec :family "Iosevka Nerd Font" :size 15)
      doom-variable-pitch-font (font-spec :family "sans" :size 15)
      doom-big-font (font-spec :family "Iosevka Nerd Font" :size 24))
(after! doom-themes
      (setq doom-themes-enable-bold t
            doom-themes-enable-italic t))
(custom-set-faces!
      '(font-lock-comment-face :slant italic)
      '(font-lock-keyword-face :slant italic))

;; Theme
(setq doom-theme 'doom-nord)

;; Org mode
(setq org-directory "~/.local/share/org/"
      org-agenda-files '("~/.local/share/org/agenda.org")
      org-default-notes-file (expand-file-name "notes.org" org-directory)
      org-journal-dir "~/.local/share/org/journal/"
      org-hide-emphasis-markers t)

;; Line number
(setq display-line-numbers-type 'relative)