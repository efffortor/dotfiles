#!/bin/sh

# Adblocking for Luakit
# run this as crontab to update adblock list regularly (daily)
# eg: @weekly ~/.local/bin/garbage/adblock_update.sh

# Ublock
curl -fsLo ~/.local/share/luakit/adblock/ublock-annoyances.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/annoyances.txt
curl -fsLo ~/.local/share/luakit/adblock/ublock-badware.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt
curl -fsLo ~/.local/share/luakit/adblock/ublock-privacy.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt
curl -fsLo ~/.local/share/luakit/adblock/ublock-resource-abuse.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/resource-abuse.txt
curl -fsLo ~/.local/share/luakit/adblock/ublock-unbreak.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/unbreak.txt
curl -fsLo ~/.local/share/luakit/adblock/ublock-filters.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters.txt
curl -fsLo ~/.local/share/luakit/adblock/ublock-filters2020.txt https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters-2020.txt

# Easylist / Fanboy
curl -fsLo ~/.local/share/luakit/adblock/easylist.txt https://easylist.to/easylist/easylist.txt
curl -fsLo ~/.local/share/luakit/adblock/easyprivacy.txt https://easylist.to/easylist/easyprivacy.txt
curl -fsLo ~/.local/share/luakit/adblock/easycookie.txt https://secure.fanboy.co.nz/fanboy-cookiemonster.txt
curl -fsLo ~/.local/share/luakit/adblock/easysocial.txt https://easylist.to/easylist/fanboy-social.txt
curl -fsLo ~/.local/share/luakit/adblock/antiadblockfilters.txt https://easylist-downloads.adblockplus.org/antiadblockfilters.txt
curl -fsLo ~/.local/share/luakit/adblock/fanboy-problematic.txt https://www.fanboy.co.nz/fanboy-problematic-sites.txt
curl -fsLo ~/.local/share/luakit/adblock/fanboy-enhanced.txt https://www.fanboy.co.nz/enhancedstats.txt
curl -fsLo ~/.local/share/luakit/adblock/fanboy-facebook.txt https://www.fanboy.co.nz/fanboy-antifacebook.txt
curl -fsLo ~/.local/share/luakit/adblock/fanboy-antifonts.txt https://www.fanboy.co.nz/fanboy-antifonts.txt
curl -fsLo ~/.local/share/luakit/adblock/fanboy-annoyance.txt https://secure.fanboy.co.nz/fanboy-annoyance.txt

# Adguard
curl -fsLo ~/.local/share/luakit/adblock/adguard-base.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_2_English/filter.txt
curl -fsLo ~/.local/share/luakit/adblock/adguard-tracking.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_3_Spyware/filter.txt
curl -fsLo ~/.local/share/luakit/adblock/adguard-social.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_4_Social/filter.txt
curl -fsLo ~/.local/share/luakit/adblock/adguard-annoyances.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_14_Annoyances/filter.txt
curl -fsLo ~/.local/share/luakit/adblock/adguard-antiadblock.txt https://raw.githubusercontent.com/AdguardTeam/AdguardFilters/master/AnnoyancesFilter/sections/antiadblock.txt

# Maliciouse hosts
curl -fsLo ~/.local/share/luakit/adblock/urlhaus-filter-online.txt https://curben.gitlab.io/malware-filter/urlhaus-filter-online.txt

# Regional
# curl -fsLo ~/.local/share/luakit/adblock/adguard-ru.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_1_Russian/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-german.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_6_German/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-fr.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_16_French/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-jp.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_7_Japanese/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-dutch.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_8_Dutch/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-spptg.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_9_Spanish/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-turk.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_13_Turkish/filter.txt
# curl -fsLo ~/.local/share/luakit/adblock/adguard-chinese.txt https://raw.githubusercontent.com/AdguardTeam/FiltersRegistry/master/filters/filter_224_Chinese/filter.txt
