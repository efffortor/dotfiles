#!/usr/bin/env bash

# a minimal approach when bash is not a default shell

export BASH_COMPLETION_USER_DIR="$HOME/.local/share/bash"
export HISTFILE="$HOME/.local/share/bash/bash_history"

# ________    _______________
# __  ___/______  /__  /___(_)_____________ ________
# _____ \_  _ \  __/  __/_  /__  __ \_  __ `/_  ___/
# ____/ //  __/ /_ / /_ _  / _  / / /  /_/ /_(__  )
# /____/ \___/\__/ \__/ /_/  /_/ /_/_\__, / /____/
#                                   /____/
# vi mode
set -o vi

# ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# Shell options
shopt -s autocd
shopt -s cdspell
shopt -s cmdhist
shopt -s dotglob
shopt -s checkwinsize
shopt -s expand_aliases
shopt -s histappend

# ________                                _____
# ___  __ \___________________ _____________  /_
# __  /_/ /_  ___/  __ \_  __ `__ \__  __ \  __/
# _  ____/_  /   / /_/ /  / / / / /_  /_/ / /_
# /_/     /_/    \____//_/ /_/ /_/_  .___/\__/
#                                 /_/
#
# get current branch in git repo
parse_git_branch() {
  BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! "${BRANCH}" = "" ]
	then
		STAT=$(parse_git_dirty)
		echo "${BRANCH}${STAT}"
	else
		echo ""
	fi
}

# get current status of git repo
parse_git_dirty() {
	status=$(git status 2>&1 | tee)
	dirty=$(echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?")
	untracked=$(echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?")
	ahead=$(echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?")
	newfile=$(echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?")
	renamed=$(echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?")
	deleted=$(echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?")
	bits=''
	if [ "${ahead}" = "0" ]; then
		bits="*${bits}"
	fi
	if [ "${renamed}" = "0" ]; then
		bits=">${bits}"
	fi
	if [ "${newfile}" = "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" = "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" = "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" = "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" = "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

# Prompt
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\]╭─[\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \$(if [[ \$? == 0 ]]; then echo \"\[$(tput setaf 2)\]\342\234\223\"; else echo \"\[$(tput setaf 1)\]\342\234\227\"; fi)\[$(tput setaf 1)\]] \[$(tput setaf 5)\]\w \[$(tput setaf 2)\]\`parse_git_branch\`\n\[$(tput setaf 1)\]╰\[$(tput sgr0)\] "
export PS2="\[\e[1;31m\]> "
