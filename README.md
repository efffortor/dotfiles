<p align="center">
    <img src="https://git.disroot.org/FollieHiyuki/dotfiles/raw/branch/master/assets/dotfiles.png" height="121px" alt="dotfiles_icon"/>
</p>

<p align="center">
    <img src="https://img.shields.io/badge/-Artix-informational?&style=for-the-badge&logo=arch-linux&logoColor=white&color=1793d1"/>
    <img src="https://img.shields.io/badge/-Void-informational?style=for-the-badge&logo=linux&logoColor=white&color=57a143"/>
    <img src="https://img.shields.io/badge/-NixOS-informational?style=for-the-badge&logo=nixos&logoColor=white&color=5277C3"/>
</p>

### :question: What can you find here

<img src="https://git.disroot.org/FollieHiyuki/dotfiles/raw/branch/master/assets/AnimuThinku.png" width="121px" align="left" alt="AnimuThinku"></a>

  **My personal dotfiles, which:**

  => is bloated (I use a lot of programs, sometimes just to try out)  
  => yet clean (follow XDG base directory specification)  
  => for [bspwm](https://github.com/baskerville/bspwm), [spectrwm](https://github.com/conformal/spectrwm), [sway](https://github.com/swaywm/sway), [wayfire](https://github.com/WayfireWM/wayfire), [river](https://github.com/ifreund/river), [hikari](https://hikari.acmelabs.space/)  
  => and features [Nord](https://github.com/arcticicestudio/nord) and [OneDark](https://github.com/joshdick/onedark.vim) colorschemes

**spectrwm**

![spectrwm](https://git.disroot.org/FollieHiyuki/dotfiles/raw/branch/master/assets/spectrwm.png)

**sway**

![sway](https://git.disroot.org/FollieHiyuki/dotfiles/raw/branch/master/assets/sway.png)

### :package: Installation

- Fonts: **Sarasa Gothic** + **Iosevka Nerd Font**  

- Other dependencies:
  - jq, sysstat, psmisc, wireless_tools (status on the bars)
  - [light](https://github.com/haikarainen/light)
  - [bottom](https://github.com/clementtsang/bottom)
  - [fzf](https://github.com/junegunn/fzf), [fd](https://github.com/sharkdp/fd), [ripgrep](https://github.com/BurntSushi/ripgrep)
  - [greenclip](https://github.com/erebe/greenclip) / [clipman](https://github.com/yory8/clipman)
  - [mpDris2](https://github.com/eonpatapon/mpDris2), [mpv-mpris](https://github.com/hoyon/mpv-mpris), [playerctl](https://github.com/altdesktop/playerctl)
  - [delta](https://github.com/dandavison/delta), [bat](https://github.com/sharkdp/bat), [glow](https://github.com/charmbracelet/glow) (or [mdcat](https://github.com/lunaryorn/mdcat)), [exa](https://github.com/ogham/exa), pandoc (previewing in terminal)
  - [starship](https://starship.rs/)
  - [gallery-dl](https://github.com/mikf/gallery-dl) (downloading mangas)
  - [pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer) or `alsa-utils`
  - [nwg-launchers](https://github.com/nwg-piotr/nwg-launchers) (used in Wayfire) or [wofi](https://hg.sr.ht/~scoopta/wofi)
  - [rofi](https://github.com/davatorium/rofi) or [dmenu](https://git.disroot.org/FollieHiyuki/dmenu)

- Run `deploy.sh` to deploy the dotfiles. It is a stupid script, so read it before running it.

> zsh requires adding `export ZDOTDIR=$HOME/.config/zsh` to **/etc/zsh/zshenv**

> Refer to [this gist](https://gist.github.com/st3r4g/6c681a28b0403b3b02636f510ff68039) for making `pipewire` work on Void Linux. The dotfiles works with `alsa` and `pulseaudio` too with some slightly changes.

### :clipboard: TODO

- [ ] ~~[Material](https://material-theme.site/) / [Ayu](https://github.com/ayu-theme/ayu-colors) palette~~
- [ ] ~~[dotdrop](https://github.com/deadc0de6/dotdrop)~~
- [x] Migrate zsh to [zinit](https://github.com/zdharma/zinit)
- [x] Wayland compositors
- [ ] NixOS / Guix / Gentoo / FreeBSD
- [ ] Rewrite Neovim's config in Lua

### :star2: Credits & License

- [@ChristianChiarulli](https://github.com/ChristianChiarulli) for the Neovim's config

- [@hlissner](https://github.com/hlissner) for a detailed zsh config (and Doom Emacs)

- My Waybar config is heavily inspired by [@begs](https://git.sr.ht/~begs/dotfiles)

- I use some scripts from other people:
    - [some fzf scripts](https://github.com/DanielFGray/fzf-scripts)
    - [colorscripts](https://gitlab.com/dwt1/shell-color-scripts)

- Icons:
    - Distro logos (for neofetch) are taken from [@owl4ce](https://github.com/owl4ce/dotfiles)
    - Icons for [ulauncher](https://github.com/Ulauncher/Ulauncher) and notifications are downloaded from [flaticon.com](https://www.flaticon.com/)

- You want some more **kawaii** wallpapers? Here is the list of artists: [@rimuu](https://rimuu.com/), [@hiten](https://www.pixiv.net/users/490219/artworks), [@Tiv](https://www.pixiv.net/en/users/35081), [@mery](https://www.pixiv.net/en/users/2750098), [@Mashima_saki](https://www.pixiv.net/en/users/18403608), [@Yuuki_Tatsuya](https://www.pixiv.net/en/users/27691), [@Bison倉鼠](https://www.pixiv.net/en/users/333556/artworks), [@Dana](https://twitter.com/hapong07), [@gomzi](https://twitter.com/gcmzi), [@Rella](https://twitter.com/Rellakinoko), [@dnwls3010](https://twitter.com/dnwls3010), [@Shigure_Ui](https://www.pixiv.net/en/users/431873), [@QuAn_](https://www.pixiv.net/en/users/6657532/artworks), [@杉87](https://twitter.com/k_su_keke1121), [@fuzichoco](https://twitter.com/fuzichoco), [@Astero](https://twitter.com/asteroid_ill), [@shin556](https://www.pixiv.net/en/users/642762)
- Others are under MIT license
